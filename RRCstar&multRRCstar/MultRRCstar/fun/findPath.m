function [path] = findPath(tree)
%find all the paths inside the tree in order to plot the tree
%
%the inputs:
%tree: the tree sturcture;
%
%the outputs:
%path: a cell array which contains all the paths

%find all the leaves inside the tree
connectingNodes = [];
for i=1:size(tree,1),
    if tree(i,end-2)==1,
        connectingNodes = [connectingNodes; tree(i,:)];
    end
end

%find all the paths from the leaves to the root
for i=1:size(connectingNodes,1)
    path{i} = connectingNodes(i,:);
    parent_node = connectingNodes(i,end);
    
    while parent_node>=1,
        path{i} = [tree(parent_node,:); path{i}];
        parent_node = tree(parent_node,end);
    end
end
