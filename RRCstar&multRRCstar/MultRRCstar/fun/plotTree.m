function plotTree(tree,figurehandle)
if nargin<1
    error('Not enough input arguments in plotTree!');
elseif nargin==1
    figure;  
    axis equal;
    hold on;
else
    figure(figurehandle);
end

% current tree
currentTreeSize=length(tree);
currentTree=tree(1:currentTreeSize);

connectingNodes = [];
for i=1:currentTreeSize,
    if isempty( currentTree(i).childID )
        connectingNodes = [connectingNodes; currentTree(i).ID];%connectingNodes include all the leaves
    end
end

% find all the pathes inside the tree
numLeaf=size(connectingNodes,1);
for i=1:numLeaf
    leafID=connectingNodes(i);
    path{i} = currentTree(leafID).position;
    parent_node = currentTree(leafID).parentID;
    while parent_node>=1,
        path{i} = [tree(parent_node).position; path{i}];
        parent_node = tree(parent_node).parentID;
    end
end

% plot paths inside the tree
numRobots = length(tree(1).position)/2;
gcf;
% axis equal;
% hold on;
for j=1:numRobots
    for i=1:length(path)
        X = path{i}(:,j);
        Y = path{i}(:,j+numRobots);
        plot(X,Y,'color',[100/255 149/255 237/255]);
    end
end
