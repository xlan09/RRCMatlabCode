function plotWorldObs(world,figurehandle)
if nargin<1
    error('Not enough input arguments in plotWorld!');
elseif nargin==1
    figure;
    axis equal;
    hold on;
    
    [numRow,numCol] = size(world.environment);
    xMax = numRow;
    yMax = numCol;
    axis([0 xMax 0 yMax]);
else
    figure(figurehandle);
end

% plot environment
environment = world.environment;
colormap(hot);
imagesc(environment');

