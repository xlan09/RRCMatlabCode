function node=generateRandomNode(world)
% generateRandomNode
% create a random node in the free space
NEcorner = world.NEcorner;
SWcorner = world.SWcorner;
NWcorner = [SWcorner(1) - abs( NEcorner(1) - SWcorner(1) );
            NEcorner(2) - abs( NEcorner(2) - SWcorner(2) )];

numRobots=world.numRobots;
while 1
    % randomly pick configuration
    pn = NWcorner(1) + abs(NEcorner(1) - SWcorner(1)) * rand(1,numRobots);% x coordinate of the random node in the 2-D plane
    pe = NWcorner(2) + abs(NEcorner(2) - SWcorner(2)) * rand(1,numRobots);% y coordinate of the random node in the 2-D plane
    
    node.ID=0;
    node.position=[pn pe];
    node.parentID=0;
    node.childID=[];
    
    % check whether the generated node collides with the obstacles
    collision_flag=collision(node, node, world);
    
    %if no collision, stop regenerating node
    if collision_flag==0
       break; 
    end
end