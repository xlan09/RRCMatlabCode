function plotCycleAndPOI(world,tree,minCycle,figurehandle)
% plot minCycle
if nargin<3
    error('Not enough input arguments in plotTree!');
elseif nargin==3
    figure;
    axis equal;
    hold on;   
    
    [numRow,numCol] = size(world.environment);
    xMax = numRow;
    yMax = numCol;
    axis([0 xMax 0 yMax]);
else
    figure(figurehandle);
    hold on;
end

% plot points of interest
for i=1:world.num_points
    plot(world.px(i),world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(world.px(i)+10,world.py(i)+10,num2str(i),'color','m','fontsize',14,'fontweight','bold');
end

% plot the cycle returned by RRC
numRobots = world.numRobots;
period=length(minCycle);
X=zeros(period,1)+1;
Y=zeros(period,1)+1;

for j=1:numRobots
    for i=1:period
        pos = tree(minCycle(i)).position;
        X(i) = pos(j);
        Y(i) = pos(j+numRobots);
    end
    X(period+1)=X(1);
    Y(period+1)=Y(1);
    plot(X,Y,'r','linewidth',2);
end