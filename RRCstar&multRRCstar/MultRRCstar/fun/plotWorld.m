function plotWorld(world,path,tree,cycleIndex)
%plot the environment, the tree and the cycle returned by RRC

numRobots=world.numRobots;
%plot the axis
sideLength=world.sideLength;
axis([world.SWcorner(1),world.NEcorner(1),...
    world.SWcorner(2), world.NEcorner(2)]);
hold on;

%plot the obstacles
for i=1:world.NumObstacles,
    X = [world.cn(i)-1/2*sideLength, world.cn(i)-1/2*sideLength, world.cn(i)+1/2*sideLength, world.cn(i)+1/2*sideLength];
    Y = [world.ce(i)-1/2*sideLength, world.ce(i)+1/2*sideLength, world.ce(i)+1/2*sideLength, world.ce(i)-1/2*sideLength];
    fill(X,Y,'k');
end

%plot the tree
for j=1:numRobots
    for i=1:size(path,2)
        X = path{i}(:,j);
        Y = path{i}(:,j+numRobots);
        plot(X,Y,'color',[100/255 149/255 237/255]);
    end
end


%plot the points of interest
for i=1:world.num_points
    plot(world.px(i),world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
end

%plot the cycle returned by RRC
period=size(cycleIndex,1);

for j=1:numRobots
    X=zeros(period+1,1);
    Y=zeros(period+1,1);
    for i=1:period
        X(i) = tree(cycleIndex(i),j);
        Y(i) = tree(cycleIndex(i),j+numRobots);
    end
    X(period+1)=X(1);
    Y(period+1)=Y(1);
    plot(X,Y,'r','linewidth',3);
end
