function cycleIndex=findCycle_v2(tree,point1_id,point2_id)
% Find the index for all the vertices inside a cycle
%
%the inputs:
% tree: the tree structure, in which the new_node has already been added to.
% point1_id: the id of the point inside the ball which is used to form the
% cycle with point2 and new_point;
% point2_id: the same with point1_id;
%
%the outputs:
%cycleIndex: the index of all the nodes inside the cycle, column vector

% path1 from point1_id to the root
parent=point1_id;
path1 = [];
while parent>=1
    path1 = [path1;parent];
    if parent == point2_id
        cycleIndex = [path1;length(tree)];
        return;
    end
    parent=tree(parent).parentID;
end

% path2 from point2_id to the root
parent=point2_id;
path2 = [];
while parent>=1
    path2 = [path2;parent];
    if parent == point1_id
        cycleIndex = [path2;length(tree)];
        return;
    end
    parent=tree(parent).parentID;
end

%find the common node of the two paths
findFlag = 0; 
path1iter = length(path1);
path2iter = length(path2);

while(path1iter >= 1 && path2iter >= 1)
    if (path1(path1iter) ~= path2(path2iter) )
        % commonAncestor is the last element equal
        commonAncestorIndexInPath1  = path1iter + 1;
        commonAncestorIndexInPath2  = path2iter + 1;
        findFlag = 1;
        break;
    end
    path1iter = path1iter - 1;
    path2iter = path2iter - 1;
end

% findFlag =0 means node1 and node2 are in the same path to root
if (findFlag == 0)
    % plus one is because it minus one in the previous loop
    commonAncestorIndexInPath1 = path1iter +1;
    commonAncestorIndexInPath2 = path2iter +1;
end

% get the cycleIndex
% plus 1 is to plus the new node, minus 1 is to minus the repeated common
% ancestor
period=commonAncestorIndexInPath1 + commonAncestorIndexInPath2 -1 +1;
cycleIndex = zeros(period,1);

% not include the common ancestor
for i =1: commonAncestorIndexInPath1-1
    cycleIndex(i) = path1(i);
end
% include the common ancestor
for i = commonAncestorIndexInPath1 : period-1
    % path2 is reversed
    cycleIndex(i) = path2(commonAncestorIndexInPath1 + commonAncestorIndexInPath2 - i); 
end
cycleIndex(period) = length(tree);