function costMex=persistentCost_mex(s,tree,world,cycleIndex)
% calculate the persistent cost of the cycle denoted by cycleIndex
%
% the tree in the input argument must be the tree which has included the
% new_node

%points of interest
numPOI = world.num_points;
POI = [world.px;
       world.py]; % this is a 2 by numPOI matrix;
POI = POI';
sigma = 100; % this is the deviation of the Gaussian when calculating C matrix
scale = 10; % this is the scale of the Gaussian when calculating C matrix
tao = 10^(-4);
numRobots = world.numRobots;
%period of the cycle
period=length(cycleIndex);

% cycle position
cyclePos=zeros(period,2*numRobots);
for i=1:period
    cyclePos(i,:)= tree(cycleIndex(i)).position;
end

%calculate cost using mex function
costMex=cost(s,cyclePos,numRobots,period,POI,numPOI,sigma,scale,tao);