%simulation for the RRC star algorithm for multiple robots
clear all;
close all;
clc;

addpath(genpath('./'))
addpath(genpath('../../learnOceanTemp'))
%*******************************************************************************
% Load environment data
environmentData = load('basisCenterLonI1200LatJ800Cluster9Wtemp0Wdist1.mat','environment','centroid');
environment = environmentData.environment;
centroid = environmentData.centroid;

%*******************************************************************************
% Load learned A, Q and R
AQRdata = load('learnAfromData.mat','A','Q','R');
A = AQRdata.A;
Q = AQRdata.Q;
R = AQRdata.R;

% Set random seed
seed=100;
str = RandStream.create('mt19937ar','seed',seed);
RandStream.setGlobalStream(str);
%**************************************
% Parameter
% Length between any two nodes.
% 200 - 250 works well
segmentLength = 250;
numRobots = 3;

numSteps=10001; % numSteps of RRC
%**************************************
%% RRCstar for multiple robots
% create the environment which needs to be monitored
world= createWorld(environment, centroid,numRobots);

%Initialize for the dynamics of the environment
s=init(A, Q, R);
%store the minCost returned at each step
start_node = generateRandomNode(world);
start_node.ID=1;

startPos=[500 200];
for i=1:numRobots
    start_node.position(1,i)=startPos(1);
    start_node.position(1,i+numRobots)=startPos(2);
end

tree=start_node;
minCycle=[];
minCycle_cost=inf;

minCostArray=zeros(numSteps,1);
Instants=zeros(numSteps,1);%Time consuming until each step

% workspace
workspaceCount=0;
interval = 500;
%calculate the time consuming
tic
for i=1:numSteps
    [tree,minCycle,minCycle_cost]= extendTree(tree,segmentLength,world,s,minCycle,minCycle_cost);
    Instants(i)=toc;
    minCostArray(i)=minCycle_cost;
    treeSize=i+1 % tree length is i+1 since we have the start node
    
    % save workspace
    if (mod(i,interval) == 0)
        filename=[num2str(i),'_RRTsegment',num2str(segmentLength),'_numRobots',num2str(numRobots),'Seed',num2str(seed)];
        workspaceName=['workspace',filename];
        save(workspaceName);
        movefile([workspaceName,'.mat'],'./workspace'); 
        workspaceCount = workspaceCount +1;
    end
end

% % remove the unncessary saved workspace
% for i =1:workspaceCount
%     filename=[num2str(i*interval),'_RRTsegment',num2str(segmentLength)];
%     workspaceName=['workspace',filename,'.mat'];
%     delete(['./workspace/',workspaceName]);
% end

%output the cost, period and the running time
filename=[num2str(numSteps),'_RRTsegment',num2str(segmentLength),'_numRobots',num2str(numRobots),'Seed',num2str(seed)];

% save workspace
workspaceName=['workspace',filename];
save(workspaceName);
movefile([workspaceName,'.mat'],'./workspace');

% save sim paramters
simInfoFilename=['simLog',filename];
diary(simInfoFilename);
numSteps
toc
minCycle_cost
period=size(minCycle,1)
A=s.A
Q=s.Q
R=s.R
disp('c~N(0,6)*10')
disp('1: without initializing algorithm.');
disp('2: without observability check.');
disp('3: without compareCost theorem.');
diary off;
movefile(simInfoFilename,'./workspace');

%% plot the world and the tree
plotWorldObs(world);
plotTree(tree,gcf);
plotCycleAndPOI(world,tree,minCycle,gcf);

% Save figure
figName=['multRRCstar',filename];
set(gcf, 'PaperPosition', [-0.48 -0.2 7 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [6.1 4.1]); %Set the paper to have width 5 and height 5.
% saveas(gcf, figName, 'png') %Save figure
set(gca, 'LooseInset', [0 0 0 0]); % get rid of the white space around the png pic
print([figName,'.png'],'-dpng','-r400')
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');

%plot how the minCycleCost changes with the iteration number
figure(3);
clf;
iterationNumber=1:numSteps;
plot(iterationNumber,minCostArray)
xlabel('Number of iterations')
ylabel('minCycleCost')
title('MinCycleCost VS. number of iterations')
% save fig
figName=['costVSiter',filename];
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

%plot how the minCycleCost changes with the computation time
figure(4);
clf;
plot(Instants,minCostArray)
xlabel('Computation time(seconds)')
ylabel('minCycleCost')
title('MinCycleCost VS. computation time')
% save fig
figName=['costVStime',filename];
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');