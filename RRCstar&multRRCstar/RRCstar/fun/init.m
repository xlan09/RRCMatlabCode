function s=init(A, Q, R)
% % dynamics of the environment
% 
% %create system matrix
% %% one option for A matrix
% % poles=[-0.5 0.8 0.6 -0.4 -0.7 0.7 -0.6 -0.4 -0.5];
% % coeff=poly(poles);
% % s.A=rot90(compan(coeff),2);
% 
% %% another option for A matrix
% s.A=eye(num_points)*0.99;
% % s.A=diag([-0.5 0.8 0.6 -0.4 -0.7 0.7 -0.6 0.3 0.2]);
% %%
% %create system noise covariance matrix
% % B=[2 1 2 1 1 1 2 1 2]';
% % s.Q=B*B';
% % s.Q=diag([50 10 50 10 10 10 50 10 50]);
% s.Q=eye(num_points)*5;
% %create sensor noise covariance matrix
% s.R=10;

s.A = A;
s.Q = Q;
s.R = R;
s.C=[];
