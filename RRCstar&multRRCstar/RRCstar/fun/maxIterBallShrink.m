function maxIter = maxIterBallShrink(world, numRobots, segmentLength)
% calculate the max iteration such that the radius of the ball is smaller
% than segmentLength

sideLength=world.sideLength;
tolerance = 0.001;

if numRobots == 1
    %volume of the free space
    freeVolume=(world.NEcorner(1)-world.SWcorner(1))* (world.NEcorner(2)-world.SWcorner(2))-world.NumObstacles*sideLength*sideLength;
    gamaRRT=2*(1+1/2)^(1/2)*(freeVolume/pi)^(1/2)+1;
    
    temp = (segmentLength/gamaRRT)^(2*numRobots);
    maxIter = 2;
    while(1)
        if maxIter - exp( maxIter*temp ) < 0 
            break;
        end
        maxIter = maxIter +1;
    end
else
    
    sampleSpaceVolume=( (world.NEcorner(1)-world.SWcorner(1))* (world.NEcorner(2)-world.SWcorner(2)) )^numRobots;
    unitBallVol = (pi^numRobots)/ factorial(numRobots);
    gamaRRT=2*(1+1/(2*numRobots))^(1/(2*numRobots))*(sampleSpaceVolume/unitBallVol)^(1/(2*numRobots));
    
    temp = (segmentLength/gamaRRT)^(2*numRobots);    
    maxIter = 2;
    while(1)
        if maxIter - exp( maxIter*temp ) < 0 
            break;
        end
        maxIter = maxIter +1
    end 
end
