// detect collision
// this function name must be mexFunction, this is like the main function
// when call this function in MATLAB,
// use detectCollision(node1Pos,node2Pos,cn,ce,numObs,sideLength)
//
// PARAMETERS:
// node1Pos and node2Pos are row vectors
// cn, ce is the x and y coordinates of the obstacles.
// numObs is the number of obstacle
// sideLength is the side length of the square obstacle.
//
// RETURN VALUE:
// collision flag=1 means collision happends, otherwise not.

#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    /* Macros for the ouput and input arguments */
#define collisionFlag_OUT plhs[0]

#define node1Pos_IN prhs[0]
#define node2Pos_IN  prhs[1]
#define cn_IN prhs[2]
#define ce_IN prhs[3]
#define numObs_IN prhs[4]
#define sideLength_IN prhs[5]

// get inputs and outputs
    double *node1Pos, *node2Pos, *cn, *ce, sideLength;
    int numObs;

    node1Pos = mxGetPr(node1Pos_IN);
    node2Pos=mxGetPr(node2Pos_IN);
    cn = mxGetPr(cn_IN);
    ce = mxGetPr(ce_IN);
    numObs=(int) mxGetScalar(numObs_IN);
    sideLength=mxGetScalar(sideLength_IN);

    double *collisionFlag;
    collisionFlag_OUT = mxCreateDoubleScalar(0);
    collisionFlag=mxGetPr(collisionFlag_OUT);

// detect collision
    int numSteps=100; // there are numSteps between 0 and 1;
    double dt;
    dt=(1.0)/(numSteps-1);
    double sigma, p1, p2;
    // note in mexFunction, mxArray is stored in column order form, this is
    // different from normal c++, see c++ coding notes.
    for (int k=0; k<numSteps; k++) {
        sigma=0+dt*k;
        p1= sigma*node1Pos[0]+(1-sigma)*node2Pos[0];
        p2= sigma*node1Pos[1]+(1-sigma)*node2Pos[1];
//            check whether it collides with each obstacle
        for (int i=0; i<numObs; i++) {
            if ( cn[i]-1.0/2*sideLength<=p1  &&  p1<=cn[i]+1.0/2*sideLength  &&  ce[i]-1.0/2*sideLength<=p2  &&  p2<=ce[i]+1.0/2*sideLength ) {
                *collisionFlag = 1;
                break;
//                %break the inter loop
            }
        }

//if it detects collision, break the outer loop
        if ( ((int)(*collisionFlag)) ==1) {
            break;
        }
    }
}
