function cost=persistentCost(s,tree,world,cycleIndex)
% calculate the persistent cost of the cycle denoted by cycleIndex
%
% the tree in the input argument must be the
%tree which has included the new_node

%period of the cycle
period=size(cycleIndex,1);
%calculate the C matrix
periodic_C=calculateCmatrix(tree,world,cycleIndex);

tao=10^(-16);%the error tolerance
PX=sscaSDA(s.A,periodic_C,s.Q,s.R,period,tao);

%[PX,K]=PQZcdc(s.A',permute(periodic_C,[2,1,3]),s.Q,s.R,period);

%find the cost from the periodic solution of the DPRE
%% one option for the cost
cost=max(eig(PX(:,:,1)));
for i=2:period
    spectralRadius=max(eig(PX(:,:,i)));
    if spectralRadius>cost
        cost=spectralRadius;
    end
end
%% another option for the cost
% cost=0;
% for i=1:period
%    cost=cost+trace(PX(:,:,i)); 
% end
% cost=cost/period;
% maxPX=PX(:,:,1);

%% third option for the cost
% sysDim=world.num_points;
% min_variance=zeros(sysDim,1);
% variance=zeros(period,1);
% for i=1:sysDim
%     
%     for j=1:period
%         covarianceMatrix=PX(:,:,j);
%        variance(j)=covarianceMatrix(i,i);
%     end
%     min_variance(i)=min(variance);
%     
% end
% 
% cost=0;
% for i=1:sysDim
%    cost=cost+min_variance(i);
% end
