function node=generateRandomNode(world)
% create a random node in the free space(initialize)
NEcorner = world.NEcorner;
SWcorner = world.SWcorner;
NWcorner = [SWcorner(1) - abs( NEcorner(1) - SWcorner(1) );
    NEcorner(2) - abs( NEcorner(2) - SWcorner(2) )];
while 1
    % randomly pick configuration
    pn = NWcorner(1) + abs(NEcorner(1) - SWcorner(1)) * rand;% x coordinate of the random node in the 2-D plane
    pe = NWcorner(2) + abs(NEcorner(2) - SWcorner(2)) * rand;
    node.ID = 0;
    node.position = [pn pe];
    node.parentID = 0;
    node.childID = [];
    
    % check whether the generated node collides with the obstacles
    collision_flag=collision(node, node, world);
    
    %if no collision, stop regenerating node
    if collision_flag==0
       break; 
    end
end