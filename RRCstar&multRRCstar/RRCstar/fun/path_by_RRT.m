function [path]=path_by_RRT(world,start_node,end_node,segmentLength)
%this function is used to find a path between the start_node and the
%end_node

%the inputs: world: the environment to do the path planning start_node:
%starting point, it must be in the last row of the current tree. end_node:
%end point segmentLength: the maximum distance between two nodes inside the
%tree

%the outputs: tree: updated tree path: a path from the end_node to the
%start_node, but not include the start_node

%initialize for the tree
tree=start_node;
%check whether the start_node can connect to the end_node directly
if ( (norm(start_node(1:2)-end_node(1:2))<segmentLength ) && (collision(end_node,start_node,world)==0) )
    %extend the tree by adding the end_node to the tree directly
    start_node(3)=0;%mark it is no longer a leaf
    end_node(5)=size(tree,1);
    tree=[tree;end_node];
else
    connection_check=0;
    while connection_check==0%if do not find the path, keep doing the loop
        
        extention_check=0;
        %if the extention of the tree is NOT successful, keep extending
        while extention_check==0
            %generate a random node
            randomPoint = generateRandomNode(world);
            randomPoint =randomPoint(1:2);
            
            % find leaf inside the tree that is closest to the randomPoint
            tmp = tree(:,1:2)-ones(size(tree,1),1)*randomPoint;
            [dist,idx] = min(diag(tmp*tmp'));
            % 'dist' is the square of the minimum distance, 'idx' is the id
            % of the nearest node.
            
            %find the potential node that will be added to the tree
            if dist<segmentLength^2
                %if the distance between the randomPoint and the nearest
                %vertex inside the tree is smaller than "segmentLength",
                %then we just choose this randomPoint as the node to add to
                %the tree; Otherwise, we can only extend the tree towards
                %this randomPoint by a maximum distance given by
                %"segmentLength"
                new_point=randomPoint;
            else
                new_edge = (randomPoint-tree(idx,1:2));
                new_point = tree(idx,1:2)+new_edge/norm(new_edge)*segmentLength;
            end
            new_node = [new_point, 1, 0, 0];
            %'1' is used to denote it is a leaf, 'idx' is the id of its
            %parent node.
            
            %check whether we can connect this new_node to the tree
            if collision(new_node, tree(idx,:), world)==0
                tree(idx,3)=0;%mark it is no longer a leaf
                new_node(5)=idx;%set the parent of the new_node
                tree = [tree; new_node];%add the new_node to the tree
                extention_check=1;
            end
        end
        
        % check to see if new node connects directly to end_node
        if ((norm(new_node(1:2)-end_node(1:2))<segmentLength )...
                &&(collision(new_node,end_node,world)==0) )
            tree(end,3)=0;%mark the new_node is no longer a leaf
            end_node(5)=size(tree,1);%set the new_node as the parent of the end_node
            tree = [tree; end_node];%add the end_node to the tree
            
            %% this part is used to find the index of the nodes in the path
            % from the end_node to the start_node
            
            path = end_node;
            parent_node = tree(end,5);
            while parent_node>1,
                path= [path;tree(parent_node,:)];
                parent_node = tree(parent_node,5);
                
            end
            connection_check = 1;%connection is completed
        end
        
    end
    
end
