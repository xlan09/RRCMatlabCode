function PX=sscaSDA(A_kalman,C_kalman,Q_kalman,R_kalman,period,tao)
% This function is used to solve the periodic DARE in the Kalman filter.

% The inputs are the periodic A, C, Q, R matrices in Kalman filter. All of
% them are 3-dimentional arrays. "period" is the period of the system.
% "tao" is the error tolerance.

% The output is the psd solution of the periodic DARE. It is also a
% 3-dimentional array.

% reference: E. K.-W. Chu, H.-Y. Fan, W.-W. Lin, C.-S. Wang,
% "Structure-Preserving Algorithms for Periodic Discrete-Time Algebraic 
% Riccati Equations",International Journal of Control,Vol. 77, Iss.8, 2004

%SSCA algorithm
dimA=size(A_kalman,1);
dimG=size(C_kalman(:,:,1),2);%the dimension of G is equal to the dimension
%of B matrix, but B is the transpose of C matrix. So it is equal to the number
%of columns in C matrix.
dimH=size(Q_kalman,1);

%A=zeros(dimA,dimA,period);
G=zeros(dimG,dimG,period);
%H=zeros(dimH,dimH,period);

%For the riccati equaiton in Kalmanf filter, the A matrix is the transpose
%of the A matrix in general riccati equation
for i=1:period
    %A(:,:,i)=A_kalman(:,:,i)';
    G(:,:,i)=C_kalman(:,:,i)'/R_kalman*C_kalman(:,:,i);
    %H(:,:,i)=Q_kalman(:,:,i);
end

A=A_kalman';%the transpose
H=Q_kalman;

%initialize G_hat,which is used to store G_hat_j and G_hat_j-1
%similarly, initialize A_hat and H_hat
A_hat=zeros(dimA,dimA,2);
G_hat=zeros(dimG,dimG,2);
H_hat=zeros(dimH,dimH,2);


A_hat(:,:,1)=A;
G_hat(:,:,1)=G(:,:,1);
H_hat(:,:,1)=H;

for i=2:period
    W=eye(dimA)+H*G_hat(:,:,1);
    V1=W\(A');
    V2=W\H;
    A_hat(:,:,2)=V1'*A_hat(:,:,1);
    G_hat(:,:,2)=G(:,:,i)+A*G_hat(:,:,1)*V1;
    H_hat(:,:,2)=H_hat(:,:,1)+A_hat(:,:,1)'*V2*A_hat(:,:,1);
    
    A_hat(:,:,1)=A_hat(:,:,2);
    G_hat(:,:,1)=G_hat(:,:,2);
    H_hat(:,:,1)=H_hat(:,:,2);
end
Ap=A_hat(:,:,2);
Gp=G_hat(:,:,2);
Hp=H_hat(:,:,2);


%SDA algorithm
%initialize G,which is used to store G_j+1 and G_j
%similarly, initialize A and H
A=zeros(dimA,dimA,2);
G=zeros(dimG,dimG,2);
H=zeros(dimH,dimH,2);

%struture-preserving doubling algorithm based on Chu's paper in 
%international journal of control, 2004
A(:,:,1)=Ap;
G(:,:,1)=Gp;
H(:,:,1)=Hp;

while 1
    W=eye(dimG)+G(:,:,1)*H(:,:,1);
    A(:,:,2)=A(:,:,1)*(W\A(:,:,1));
    G(:,:,2)=G(:,:,1)+A(:,:,1)*(G(:,:,1)/(W'))*A(:,:,1)';
    H(:,:,2)=H(:,:,1)+(W\A(:,:,1))'*H(:,:,1)*A(:,:,1);
    
    if norm(H(:,:,2)-H(:,:,1),'fro')<=tao*norm(H(:,:,2),'fro')
        X=H(:,:,2);
        break;
    else
        A(:,:,1)=A(:,:,2);
        G(:,:,1)=G(:,:,2);
        H(:,:,1)=H(:,:,2);
    end
end

%calculate the priodic psd solution of the periodic DARE
PX=zeros(dimA,dimA,period);
PX(:,:,period)=X;
for i=period:-1:2
   PX(:,:,i-1)=A_kalman*PX(:,:,i)*A_kalman'...
       -A_kalman*PX(:,:,i)*C_kalman(:,:,i)'/(C_kalman(:,:,i)*PX(:,:,i)*C_kalman(:,:,i)'+R_kalman)...
      *C_kalman(:,:,i)*PX(:,:,i)*A_kalman'+Q_kalman;
end
