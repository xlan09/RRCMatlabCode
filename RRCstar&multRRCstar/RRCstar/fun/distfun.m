function out = distfun(center, data)
% calculates the Euclidean distance
%	between each row in CENTER and each row in DATA, and returns a
%	distance matrix OUT of size M by N, where M and N are row
%	dimensions of CENTER and DATA, respectively, and OUT(I, J) is
%	the distance between CENTER(I,:) and DATA(J,:).
% 
% center: M by n matrix, n is the dimension of the data. E.g. if n=2, means
% center(i,:) is one point in the plane.
% data: N by n matrix
out = zeros(size(center, 1), size(data, 1));

% fill the output matrix
if size(center, 2) > 1,
    for k = 1:size(center, 1),
	out(k, :) = sqrt(sum(((data-ones(size(data, 1), 1)*center(k, :)).^2)'));
    end
else	% 1-D data
    for k = 1:size(center, 1),
	out(k, :) = abs(center(k)-data)';
    end
end