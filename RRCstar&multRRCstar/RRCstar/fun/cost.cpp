// calculate cost using ssca and sda
// this function name must be mexFunction, this is like the main function
// when call this function in MATLAB,
// use cost(s,cyclePos,numSensors,period,POI,numPOI,sigma,scale,tao)
//
// PARAMETERS:
// s is a structure which contains info about the A, Q, R matrix
// cyclePos is a period by 2*numSensors matrix, while the first 0 : 1 columns
// is the first cycle by sensor1, 2:3 columns is the second cycle by sensor2,......
// POI is point of interest, which is a numPOI by 2 matrix
// numPOI is the number of point interest
// sigma is the deviation of the Gaussian
// tao is the error tolerance of the SDA algorithm
//
// RETURN VALUE:
// cost, which is the largest spectral radius of all the asymptotic covariance
// matrix along the cycle.

#include "mex.h"
#include <math.h> //exp
#include "Eigen/Dense"
#include <iostream>

using namespace std;
using namespace Eigen;

double maxInVector(VectorXd vec) {
    if (vec.size()) {
        double max;
        max=vec(0);
        for (int i=1; i<vec.size(); i++) {
            if(vec(i)>max) {
                max=vec(i);
            }
        }
        return max;
    } else {
        cout<<"Empty vector!!!!!!!!!"<<endl;
        return 0; //empty vector
    }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
//  Macros for the ouput and input arguments
#define s_IN prhs[0]
#define cyclePos_IN  prhs[1]
#define numSensors_IN  prhs[2]
#define period_IN prhs[3]
#define POI_IN prhs[4]
#define numPOI_IN prhs[5]
#define sigma_IN prhs[6]
#define scale_IN prhs[7]
#define tao_IN prhs[8]

#define cost_OUT plhs[0]
// get inputs and outputs
    double *Apr,*Qpr,*Rpr,*cyclePos, *POI, sigma, scale, tao;
    int period, numPOI, numSensors;

// get A, Q, R
    mxArray *A_IN;
    A_IN = mxGetField(prhs[0], 0,"A");
    Apr=mxGetPr(A_IN);

    mxArray *Q_IN;
    Q_IN = mxGetField(prhs[0], 0,"Q");
    Qpr=mxGetPr(Q_IN);

    mxArray *R_IN;
    R_IN = mxGetField(prhs[0], 0,"R");
    Rpr=mxGetPr(R_IN);
// get cycle
    cyclePos= mxGetPr(cyclePos_IN);
    numSensors=(int) mxGetScalar(numSensors_IN);
    period=(int) mxGetScalar(period_IN);
    POI = mxGetPr(POI_IN);
    numPOI=(int) mxGetScalar(numPOI_IN);
    sigma=mxGetScalar(sigma_IN);
    scale=mxGetScalar(scale_IN);
    tao=mxGetScalar(tao_IN);

// calculate periodic C matrix
    MatrixXd Cmatrix(numSensors,numPOI);

    MatrixXd *periodic_C= new MatrixXd[period];

    double sensorPos[2];
    for (int k=0; k<period; k++) {
        for(int i=0; i<numSensors; i++) {
            sensorPos[0]=cyclePos[k + i * period];
            sensorPos[1]=cyclePos[k + i * period + numSensors * period];
            for(int j=0; j<numPOI; j++) {
                // mxArray is stored in column order.
                Cmatrix(i,j)=scale * exp( (-1.0)* ( (sensorPos[0]-POI[j])*(sensorPos[0]-POI[j])  + (sensorPos[1]-POI[j+numPOI])* (sensorPos[1]-POI[j+numPOI]) ) / (2*sigma*sigma));
            }
        }
        periodic_C[k]=Cmatrix;
    }

//    // output matrix
//    for(int i=0; i<period; i++) {
//        Cmatrix=periodic_C[i];
//        for(int j=0; j<numSensors; j++) {
//            for (int k=0; k<numPOI; k++) {
//                mexPrintf("%f\t",Cmatrix(j,k));
//            }
//            mexPrintf("\n");
//        }
//        cout<<"-------------------------"<<endl;
//    }

//    ssca + sda algorithm
    /* % The inputs are the periodic A, C, Q, R matrices in Kalman filter. All of
     * % them are 3-dimentional arrays. "period" is the period of the system.
     * % "tao" is the error tolerance.
     *
     * % The output is the psd solution of the periodic DARE. It is also a
     * % 3-dimentional array.
     *
     * % reference: E. K.-W. Chu, H.-Y. Fan, W.-W. Lin, C.-S. Wang,
     * % "Structure-Preserving Algorithms for Periodic Discrete-Time Algebraic
     * % Riccati Equations",International Journal of Control,Vol. 77, Iss.8, 2004
     */

// get A, Q, R;
    MatrixXd A(numPOI,numPOI);
    MatrixXd Q(numPOI,numPOI);
    MatrixXd H(numPOI,numPOI);
    MatrixXd R=MatrixXd::Zero(numSensors,numSensors);
    for (int i=0; i<numPOI; i++) {
        for (int j=0; j<numPOI; j++) {
            A(i,j)=Apr[i+j*numPOI];
            Q(i,j)=Qpr[i+j*numPOI];
            H(i,j)=Q(i,j);
        }
    }

    for (int i=0; i<numSensors; i++) {
        R(i,i)=*Rpr;
    }
//    Get G
    int dimA=numPOI, dimG=numPOI, dimH=numPOI;
    MatrixXd *G= new MatrixXd[period];
    for (int i = 0; i < period; i++) {
        G[i]=periodic_C[i].transpose() * R.inverse() * periodic_C[i];
    }
// transpose A
    A.transposeInPlace(); // node A becomes its transpose. In Eigen, do NOT do this A=A.transpose(), use A.transposeInPlace();
// initialization
    MatrixXd *A_hat= new MatrixXd[2];
    for (int i = 0; i < 2; i++) {
        A_hat[i]=MatrixXd::Zero(dimA,dimA);
    }
    MatrixXd *G_hat= new MatrixXd[2];
    for (int i = 0; i < 2; i++) {
        G_hat[i]=MatrixXd::Zero(dimG,dimG);
    }
    MatrixXd *H_hat= new MatrixXd[2];
    for (int i = 0; i < 2; i++) {
        H_hat[i]=MatrixXd::Zero(dimH,dimH);
    }

    A_hat[0]=A;
    G_hat[0]=G[0];
    H_hat[0]=H;

    MatrixXd W(dimA,dimA);
    MatrixXd V1(dimA,dimA);
    MatrixXd V2(dimA,dimA);
    for(int i=1; i<period; i++) {
        W=MatrixXd::Identity(dimA,dimA)+H*G_hat[0];
        V1=W.inverse() * A.transpose();
        V2=W.inverse() * H;
        A_hat[1]=V1.transpose() * A_hat[0];
        G_hat[1]=G[i]+ A * G_hat[0] * V1;
        H_hat[1]=H_hat[0]+A_hat[0].transpose() * V2 * A_hat[0];

        A_hat[0]=A_hat[1];
        G_hat[0]=G_hat[1];
        H_hat[0]=H_hat[1];
    }

    /* SDA algorithm */
    A_hat[0]=A_hat[1];
    G_hat[0]=G_hat[1];
    H_hat[0]=H_hat[1];

    while (1) {
        W=MatrixXd::Identity(dimG,dimG)+G_hat[0] * H_hat[0];
        A_hat[1]=A_hat[0] * ( W.inverse() * A_hat[0] );
        G_hat[1]=G_hat[0] + A_hat[0] * ( G_hat[0] * (W.transpose()).inverse() ) * A_hat[0].transpose();
        H_hat[1]=H_hat[0] + A_hat[0].transpose() * (W.transpose()).inverse() * H_hat[0] * A_hat[0];
        // In the original paper, the stopping criterion is (H_hat[1]-H_hat[0]).norm() <=  tao * H_hat[1].norm()
        // Here I make it (H_hat[1]-H_hat[0]).norm() <=  tao
        if ( (H_hat[1]-H_hat[0]).norm() <=  tao ) {
            break;
        } else {
            A_hat[0]=A_hat[1];
            G_hat[0]=G_hat[1];
            H_hat[0]=H_hat[1];
        }
    }

    /* calculate the periodic solutions of the DARE */
// In order to save memory, here we use G to store the asymptotic covariance matrix. Here G is the PX in sscaSDA.m
    G[period-1]=H_hat[1];

// transpose A back
    A.transposeInPlace();

    for (int i=period-1; i>0; i--) {
        G[i-1]=A * G[i] * A.transpose() - A * G[i] *periodic_C[i].transpose() * (periodic_C[i]*G[i]* periodic_C[i].transpose() + R).inverse() * periodic_C[i] * G[i] * A.transpose() + Q;
    }

// get the cost
    cost_OUT=mxCreateDoubleScalar(0);
    double *cost, specRadius;
    cost=mxGetPr(cost_OUT);

    VectorXcd eigenVal;
    VectorXd real;
    eigenVal=G[0].eigenvalues();
    real=eigenVal.real();
    *cost=maxInVector(real);

    for (int i=1; i<period; i++) {
        eigenVal=G[i].eigenvalues();
        real=eigenVal.real();
        specRadius=maxInVector(real);
        if (specRadius> *cost) {
            *cost=specRadius;
        }
    }
    delete[] periodic_C;
    delete[] G;
    delete[] A_hat;
    delete[] G_hat;
    delete[] H_hat;
}
