function [path] = findPath(tree)
%find all the paths inside the tree

leafNodes = [];
for i=1:length(tree),
    if isempty(tree(i).childID)
        leafNodes = [leafNodes; tree(i)];
        %connectingNodes include all the leaves
    end
end

%find all the paths inside the tree
for i=1:length(leafNodes)
    path{i} = leafNodes(i);
    parent_node = leafNodes(i).parentID;
    
    while parent_node>=1,
        path{i} = [tree(parent_node); path{i}];
        parent_node = tree(parent_node).parentID;
    end
end
