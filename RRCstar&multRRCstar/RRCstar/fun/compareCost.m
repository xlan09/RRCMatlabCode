function flag=compareCost(s,tree,world,cycleIndex,sigma0)
%this function is used to comapre the cost of two cycles while knwoing the
%cost of one cycle, denoted by "sigma0"
%
%the inputs:
%sigma0: the cost of one cycle
%cycleIndex: the cycle used to compare
%
%the output:
% flag=1 means all the sigma matrix are smaller than sigma0 after one
% period; that is, the cost of the cycle is smaller than sigma0.
%flag=0 means there exists at least one sigma matrix which is greater than
%sigma0 after one period.
%flag=-1 means we can NOT decide after one period


%period of the cycle
period=size(cycleIndex,1);

%get an array to store the covariance matrix
num_points=world.num_points;
SIGMA=zeros(num_points,num_points,period);

%calculate the sigma matrix after one period for each node inside the cycle
for i=1:period
    post_sigma=sigma0;
    for j=1:period
        [post_sigma]=kalman(s,world,tree(cycleIndex(j),1:2),post_sigma);
    end
    cycleIndex=cycleIndex([end,1:end-1]);
    SIGMA(:,:,i)=post_sigma;
end

%compare cost
smaller_count=0;
for i=1:period
    eigen=eig(SIGMA(:,:,i)-sigma0);
    if min(eigen)>=0
        %flag=0 means there exists at least one sigma matrix greater than sigma0
        flag=0;
        %return means we already know that the cycle denoted by indexCycle
        %has larger cost than sigma0
        return;
    elseif max(eigen)<0
        smaller_count=smaller_count+1;        
    else
        %do nothing
    end
end
    
%     if max(eig(SIGMA(:,:,i)))> max(eig(sigma0))
%         %flag=0 means there exits at least one SIGMA greater than sigma0
%         flag=0;
%         cost_by_bruteForce=max(eig(SIGMA(:,:,i)))
%         break;
%     end

if smaller_count==period
    flag=1;%flag=1 means the cycle denoted by cycleIndex has smaller cost
else
    %flag=-1 means we cannot decide whether the cycle denoted by cycleIndex
    %has smaller or larger cost than sigma0 after one perioid, because
    %there exists at least one node at which the covariance matrix
    %{SIGMA(:,:,i)-sigma0} is indefinite and at other nodes the covariance
    %matrix {SIGMA(:,:,i)-sigma0} is negative definite
    flag=-1;
end
