function [new_tree,minCycle,minCycle_cost]= extendTree(tree,segmentLength,world,s,minCycle,minCycle_cost)
%   Extend the tree by randomly selecting point and growing tree toward
%   that point "segmentLength" is the maximum length between two connected
%   nodes inside the tree; "minCylce" is the index of the cycle inside the
%   tree which has the minimum cost; "minCycle_cost" is the cost of the
%   minCycle

%volume of the free space
volume = abs(world.NEcorner(1)-world.SWcorner(1)) * abs(world.NEcorner(2)-world.SWcorner(2));
gamaRRT=2*(1+1/2)^(1/2)*(volume/pi)^(1/2);
% tree size
currentTreeSize=length(tree);

%extention_flag used to denote whether we extend the tree successfully. If
%NOT, we need to generate random node again until we extend it successfully
extention_flag = 0;
while extention_flag==0,
    % select a random point
    new_node = generateRandomNode(world);
    new_node.ID=currentTreeSize+1;
    randomPoint =new_node.position;
    
    % find node inside the tree that is closest to the randomNode
    points=[tree.position];
    pointsDim=length(tree(1).position);
    points=reshape(points,pointsDim,currentTreeSize);
    points=points'; % this is a currentTreeSize by pointsDim matrix
    % distance between the randomPos and all the vertices inside the tree
    points2RandomNodeDist=distfun(randomPoint,points); %randomPos must be a row vector
    points2RandomNodeDist=points2RandomNodeDist';% make sure it is a column vector
    % 'dist' is the minimum distance, 'idx' is the id of the nearest node.
    [dist,idx] = min(points2RandomNodeDist);

    if dist<segmentLength
        %if the distance between the randomPoint and the nearest vertex
        %inside the tree is smaller than "segmentLength", then we just
        %choose this randomPoint as the node to add to the tree; Otherwise,
        %we can only extend the tree towards this randomPoint by a maximum
        %distance given by "segmentLength"
        new_point=randomPoint;
    else
        new_edge = randomPoint-tree(idx).position;
        new_point = tree(idx).position+new_edge/norm(new_edge)*segmentLength;
    end
    
    %potential node that will be added to the tree
    new_node.position = new_point;
    
    %% find which vertex we should connect this new_node to.    
    if collision(new_node, tree(idx), world)==0
        %radius of the RRT*
        radius=min(gamaRRT*(log(size(tree,1))/size(tree,1))^(1/2),segmentLength);
        points2new_nodeDist=distfun(new_point,points); 
        points2new_nodeDist=points2new_nodeDist';% make sure it is a column vector
        
        IDX=[];%this is used to store all the index of the vertices
        %inside the ball centered at the new_point with the above radius
        for i=1:length(points2new_nodeDist)
            if points2new_nodeDist(i)<radius
                IDX=[IDX;i];
            end
        end
        IDXsize=size(IDX,1)     
        
        % add the potential nodes to the tree
        new_tree = [tree; new_node];
        if IDXsize <= 1
            %if there is only one vertex inside the ball, it must be the
            %nearest vertex. Just conncet the new_node to this nearest
            %vertex
            new_tree(currentTreeSize+1).parentID =idx; %set the parent node
            new_tree(idx).childID=[new_tree(idx).childID;currentTreeSize+1];
        else
            % if there are at least 2 vertices inside the near ball
            rewireFlag=0;
            indexOfWaypointsInsideBall=[];
            indexInsideMinCyle=[];
            for iter1=1:length(minCycle)
                for iter2=1:IDXsize
                    if minCycle(iter1) == IDX(iter2)
                        indexOfWaypointsInsideBall=[indexOfWaypointsInsideBall;minCycle(iter1)];
                        % indexInsideMinCyle must be a sorted ascending column vector,
                        % since we will use this in the following loop when
                        % considering the new potential minCycle
                        indexInsideMinCyle=[indexInsideMinCyle;iter1]; 
                    end
                end
            end
            % if there are at least two waypoints of the current minCycle inside the ball
            numWaypointsInsideBall=length(indexInsideMinCyle);
            if  numWaypointsInsideBall>1
                rewireMinCost=minCycle_cost;
                rewireMinCycle=[];
                for i=1:numWaypointsInsideBall-1
                    for j=i+1:numWaypointsInsideBall
                        if collision(new_node,tree(minCycle(indexInsideMinCyle(i))),world)==0 && ...
                                collision(new_node,tree(minCycle(indexInsideMinCyle(j))),world)==0
                            % currentTreeSize+1 is the id of the new random node
                            potentialMincycle=[minCycle(1:indexInsideMinCyle(i));currentTreeSize+1;minCycle( indexInsideMinCyle(j):end )];
                            cycle_cost=persistentCost_mex(s,new_tree,world,potentialMincycle);
                            if cycle_cost<rewireMinCost
                                rewireMinCost=cycle_cost;
                                rewireMinCycle=potentialMincycle;
                                rewireWaypoints=[minCycle(indexInsideMinCyle(i));minCycle(indexInsideMinCyle(j))];
                                rewireFlag=1;
                            end
                        end
                    end
                end
            end
%*******************************************************************************
            if rewireFlag == 0
                % normal RRC****************************************************
                min_id=idx;%assume that the nearest vertex has the minimum cost
                %"idx" is the id of the nearest vertex.
                min_cost=inf;
                indexOfCycle=[];
                
                newTreeSize=length(new_tree);
                %% check detectability and compare the cost of these cycles
                for i=1:IDXsize-1
                    for j=(i+1):IDXsize
                        if collision(new_node,tree(IDX(i)),world)==0 && collision(new_node,tree(IDX(j)),world)==0
                            %find the index of the cycle
                            cycleIndex=findCycle_v2(new_tree,IDX(i),IDX(j));
%                             cycleIndex=findCycle_mex(new_tree,newTreeSize,IDX(i),IDX(j))
                            cycle_cost=persistentCost_mex(s,new_tree,world,cycleIndex);
                            
                            if  cycle_cost<min_cost
                                min_cost=cycle_cost;
                                min_id=IDX(j);
                                indexOfCycle=cycleIndex;
                            end
                            
                        end
                    end
                end
                    new_tree(newTreeSize).parentID =min_id; %set the parent node
                    new_tree(min_id).childID=[new_tree(min_id).childID;newTreeSize];
                
                %Find the min cost cycle in the tree
                if  min_cost<minCycle_cost
                    minCycle_cost=min_cost;
                    minCycle=indexOfCycle;
                end   
                
            else
                % if rewireFlag=1, it means rewireMinCost is smaller than
                % the original minCycle_cost
                
                % still execute normal RRC and see whether normal RRC can
                % give lower cost than rewire
                
                min_id=idx;%assume that the nearest vertex has the minimum cost
                %"idx" is the id of the nearest vertex.
                min_cost=inf;
                indexOfCycle=[];
                
                newTreeSize=length(new_tree);
                %% compare the cost of these cycles
                for i=1:IDXsize-1
                    for j=(i+1):IDXsize
                        if collision(new_node,tree(IDX(i)),world)==0 && collision(new_node,tree(IDX(j)),world)==0
                            %find the index of the cycle
                            cycleIndex=findCycle_v2(new_tree,IDX(i),IDX(j));
%                             cycleIndex=findCycle_mex(new_tree,newTreeSize,IDX(i),IDX(j))
                            cycle_cost=persistentCost_mex(s,new_tree,world,cycleIndex);
                            
                            if  cycle_cost<min_cost
                                min_cost=cycle_cost;
                                min_id=IDX(j);
                                indexOfCycle=cycleIndex;
                            end   
                        end
                    end
                end
                
                %compare the cost by normal RRC with the cost of rewire
                if  min_cost<rewireMinCost
                    minCycle_cost=min_cost;
                    minCycle=indexOfCycle;
                    new_tree(newTreeSize).parentID =min_id; %set the parent node
                    new_tree(min_id).childID=[new_tree(min_id).childID;newTreeSize];
                else % do rewire
                    minCycle_cost=rewireMinCost;
                    minCycle=rewireMinCycle;
                    [parentNodeIndex, childNodeIndex, isInSamePathFlag]=isInSamePath(new_tree, rewireWaypoints);

                    if isInSamePathFlag
                        new_tree(rewireWaypoints(parentNodeIndex)).childID=[new_tree(rewireWaypoints(parentNodeIndex)).childID;newTreeSize];
                        new_tree(newTreeSize).parentID=rewireWaypoints(parentNodeIndex);% set parent
                        % delete from old parent children vector
                        oldParentID=new_tree(rewireWaypoints(childNodeIndex)).parentID;
                        childIDVec=new_tree(oldParentID).childID;
                        new_tree(oldParentID).childID=childIDVec(childIDVec~=rewireWaypoints(childNodeIndex));
                        new_tree(rewireWaypoints(childNodeIndex)).parentID=newTreeSize;
                        new_tree(newTreeSize).childID = [new_tree(newTreeSize).childID;rewireWaypoints(childNodeIndex)];
                    else
                        % choose any one from rewireWaypoints as the parent
                        new_tree(rewireWaypoints(1)).childID=[new_tree(rewireWaypoints(1)).childID;newTreeSize];
                        new_tree(newTreeSize).parentID=rewireWaypoints(1);% set parent
                        
                        oldParentID=new_tree(rewireWaypoints(2)).parentID;
                        childIDVec=new_tree(oldParentID).childID;
                        new_tree(oldParentID).childID=childIDVec(childIDVec~=rewireWaypoints(2));
                        new_tree(rewireWaypoints(2)).parentID=newTreeSize; 
                        new_tree(newTreeSize).childID = [new_tree(newTreeSize).childID;rewireWaypoints(2)];
                    end 
                end               
            end           
        end
        extention_flag=1;%it is used to denote we have extended the tree
    end
end

end

function [parentNodeIndex, childNodeIndex, isInSamePathFlag]=isInSamePath(tree, rewireWaypoints)
    % find whether the two nodes are in the same path: if yes, return 1 and 
    % return which one is the parent; if not, return 0
    node1ID=rewireWaypoints(1);
    node2ID=rewireWaypoints(2);

    % find whether node2ID is the parent of node1ID
    parent=node1ID;
    while parent>=1
        if (parent == node2ID)
            isInSamePathFlag = 1;
            parentNodeIndex=2; % this means node2ID is the parent
            childNodeIndex=1;
            return;
        end
        parent=tree(parent).parentID;
    end

    % find whether node1ID is the parent of node2ID    
    parent=node2ID;
    while parent>=1
        if (parent == node1ID)
            isInSamePathFlag = 1;
            parentNodeIndex=1; % this means node1ID is the parent
            childNodeIndex=2;
            return;
        end        
        parent=tree(parent).parentID;        
    end
    
    % if node1ID and node2ID are NOT in the same path
        isInSamePathFlag=0;
        parentNodeIndex=inf; % here assign inf to it on purpose, used to debug
        childNodeIndex=inf;
end