clear all;
close all;
clc;

addpath(genpath('./'))
addpath(genpath('../RRC&multRRC/RRC/fun/')) % collision, kalman
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function

seed = 5000;
str = RandStream.create('mt19937ar','seed',seed);
RandStream.setGlobalStream(str);
%*******************************************************************************
% numSteps
numSteps = 1000;
randMat = rand(9,9)*3;
initCovMat = randMat' * randMat;
numActions = 8; % number of actions in greedy algorithm
numStages = 5; % num of Stages in receding horizon
initPos = [500 200]; % init pos of random search and greedy
%*******************************************************************************
% find RRC waypoints
data = load('../RRC&multRRC/RRC/workspace/workspace10001_RRTsegment50Seed5489.mat','minCycle','tree','world','segmentLength','s');
RRCcycle = data.minCycle;
RRCtree = data.tree;
world = data.world;
sys = data.s;

segmentLength = data.segmentLength;
RRCwaypoints = zeros(numSteps, 2);
period = length(RRCcycle);

oneCyclePos = zeros(period, 2);
for i = 1:period
    oneCyclePos(i, :) = RRCtree(RRCcycle(i), 1:2);
end

numPeriod = floor(numSteps / period);
RRCwaypoints(1:numPeriod * period, :) = repmat(oneCyclePos, numPeriod, 1);
for i = numPeriod * period + 1 : numSteps
    RRCwaypoints(i, :) = oneCyclePos(i - numPeriod * period, :);
end

% ******************************************************************************
% find RRCstar waypoints
data = load('../RRCstar&multRRCstar/RRCstar/workspace/workspace10001_RRTsegment50Seed5489.mat','minCycle','tree');
RRCstarCycle = data.minCycle;
RRCstarTree = data.tree;

RRCstarWaypoints = zeros(numSteps, 2);
period = length(RRCstarCycle);

oneCyclePos = zeros(period, 2);
for i = 1:period
    oneCyclePos(i, :) = RRCstarTree(RRCstarCycle(i)).position;
end

numPeriod = floor(numSteps / period);
RRCstarWaypoints(1:numPeriod * period, :) = repmat(oneCyclePos, numPeriod, 1);
for i = numPeriod * period + 1 : numSteps
    RRCstarWaypoints(i, :) = oneCyclePos(i - numPeriod * period, :);
end

% ******************************************************************************
% initialization
RSwaypoints = zeros(numSteps, 2);
RSwaypoints(1,:) = initPos;
RScost = zeros(numSteps, 1);
RScost(1) = max(eig(initCovMat));
RScurrentPos = RSwaypoints(1,:);
RScurrentCovMat = initCovMat;

RRCcost  = zeros(numSteps, 1);
RRCcost(1) = max( eig(initCovMat) );
RRC_CovMat = initCovMat; 
RRCpos = RRCwaypoints(1, :);

RRCstarCost  = zeros(numSteps, 1);
RRCstarCost(1) = max( eig(initCovMat) );
RRCstar_CovMat = initCovMat; 
RRCstarPos = RRCstarWaypoints(1, :);

% greedy algorithm initialization
GdyWaypoints = zeros(numSteps, 2);
GdyWaypoints(1, :) = initPos;
GdyCost = zeros(numSteps, 1);
GdyCost(1) = max(eig(initCovMat));
Gdy_CrtPos = GdyWaypoints(1, :);
Gdy_CrtCovMat = initCovMat;


% calculate cost
for iter = 2:numSteps
    %**************************************
    % RRC
    RRCpos = RRCwaypoints(iter,:);
    RRCnextCovMat = kalman(sys,world, RRCpos, RRC_CovMat);
    RRCcost(iter) = max( eig(RRCnextCovMat) );
    RRC_CovMat = RRCnextCovMat;
    
    %**************************************
    % RRC star
    RRCstarPos = RRCstarWaypoints(iter,:);
    RRCstarNextCovMat = kalman(sys,world, RRCstarPos, RRCstar_CovMat);
    RRCstarCost(iter) = max( eig(RRCstarNextCovMat) );
    RRCstar_CovMat = RRCstarNextCovMat;
    
    
    %*************************************
    % random search
    extention_flag = 0;
    while extention_flag==0,
        % select a random point
        randomPoint = generateRandomNode(world);
        randomPoint =randomPoint(1:2);
        
        dist = norm(RScurrentPos - randomPoint);
        if dist <= segmentLength
            new_point = randomPoint;
        else
            new_edge = (randomPoint - RScurrentPos);
            new_point = RScurrentPos + new_edge/norm(new_edge) * segmentLength;
        end
        
        if collision(new_point, RScurrentPos, world) == 0
            RSwaypoints(iter,:) = new_point; 
            extention_flag = 1;
        end        
    end
    
    RSnextCovMat = kalman(sys,world,new_point,RScurrentCovMat);
    RScost(iter) = max( eig(RSnextCovMat) );
    RScurrentPos = new_point;
    RScurrentCovMat = RSnextCovMat;
    
    %******************************************
    % Greedy algorithm
    GdyMinCost = inf;
    stuckFlag = 1;
    for i = 1:numActions
        e = [cos( (i -1) * (2*pi/numActions) ), sin( (i -1) * (2*pi/numActions) )];
        newPos = Gdy_CrtPos + e * segmentLength;
        if collision(newPos, Gdy_CrtPos, world) == 0
            GdyNextCovMat = kalman(sys,world,newPos,Gdy_CrtCovMat);
            GdyTempCost = max( eig(GdyNextCovMat) );
            
            if GdyTempCost < GdyMinCost
                GdyMinCost = GdyTempCost;
                nextPos = newPos;
                nextCovMat = GdyNextCovMat;
                stuckFlag = 0;
            end
        end
    end
    
    % if stuck, randomly choose one direction to escape
    if stuckFlag == 1
        escapeFlag = 0;
        while escapeFlag == 0
            randomAngle = rand * 2 * pi;
            unitDirect = [cos(randomAngle), sin(randomAngle)];
            newPos = Gdy_CrtPos + unitDirect * segmentLength;
            if collision(newPos, Gdy_CrtPos, world) == 0
                escapeFlag = 1;
                GdyNextCovMat = kalman(sys,world,newPos,Gdy_CrtCovMat);
                GdyMinCost = max( eig(GdyNextCovMat) );
                nextPos = newPos;
                nextCovMat = GdyNextCovMat;
            end
        end        
    end
    
    GdyCost(iter) = GdyMinCost;
    GdyWaypoints(iter,:) = nextPos;
    Gdy_CrtCovMat = nextCovMat;
    Gdy_CrtPos = nextPos;
    
    iter
end

filename = ['workspaceGreedyRS',num2str(numSteps),'NumActions',num2str(numActions),'InitPos',num2str(initPos(1)),'and',num2str(initPos(2))];
save(filename);
movefile([filename, '.mat'], './workspace');

%% 
% Receding horizon, it is very slow, so I put it here separately. But it can also be
% put in the same loop as random search and greedy in the above code
RHWaypoints = zeros(numSteps,2);
RHWaypoints(1, :) = initPos;
RHCost = zeros(numSteps, 1);
RHCost(1) = max(eig(initCovMat));
RHCrtPos = RHWaypoints(1,:);
RHCrtCovMat = initCovMat;


% calculate cost
for iter = 2:numSteps    
    %**********************************************
    % Receding horizon
    tic
    [action2Take, stuckFlag] = recedingHorizon(sys,world, RHCrtPos, RHCrtCovMat, numActions,numStages, segmentLength);
    toc
    % if stuck, randomly choose one direction to escape
    if stuckFlag == 1
        escapeFlag = 0;
        while escapeFlag == 0
            randomAngle = rand * 2 * pi;
            unitDirect = [cos(randomAngle), sin(randomAngle)];
            newPos = RHCrtPos + unitDirect * segmentLength;
            if collision(newPos, RHCrtPos, world) == 0
                escapeFlag = 1;
                RHNextCovMat = kalman(sys,world,newPos,RHCrtCovMat);
                RHMinCost = max( eig(RHNextCovMat) );
                nextPos = newPos;
                nextCovMat = RHNextCovMat;
            end
        end
        
    else
        e = [cos( (action2Take(1) -1) * (2*pi/numActions) ), sin( (action2Take(1) -1) * (2*pi/numActions) )];
        newPos = RHCrtPos + e * segmentLength;
        
        nextCovMat = kalman(sys,world,newPos,RHCrtCovMat);
        RHMinCost = max( eig(nextCovMat) );
        nextPos = newPos;
    end
    
    RHCost(iter) = RHMinCost;
    RHWaypoints(iter,:) = nextPos;
    RHCrtCovMat = nextCovMat;
    RHCrtPos = nextPos;
    
    iter
end

filename = ['workspaceGreedyRSRH',num2str(numSteps),'NumActions',num2str(numActions),'Horizon',num2str(numStages),'InitPos',num2str(initPos(1)),'and',num2str(initPos(2))];
save(filename);
movefile([filename, '.mat'], './workspace');

%% claculate cost along TSP tour
clear all;
close all;
addpath(genpath('./'))
addpath(genpath('../RRC&multRRC/RRC/fun/')) % collision, kalman
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function
workspaceFilenameToLoad = 'workspaceGreedyRSRH3000NumActions8Horizon4InitPos500and200';
load(['./workspace/', workspaceFilenameToLoad, '.mat']);
% TSP tour
TSPtour = load('./workspace/workspaceTSPtourNumIter5000RRTsegment50Seed5000.mat','TSPtour');
TSPtour = TSPtour.TSPtour;
TSPtourWaypoints = zeros(numSteps, 2);
period = size(TSPtour, 1);

numPeriod = floor(numSteps / period);
TSPtourWaypoints(1:numPeriod * period, :) = repmat(TSPtour, numPeriod, 1);
for i = numPeriod * period + 1 : numSteps
    TSPtourWaypoints(i, :) = TSPtour(i - numPeriod * period, :);
end

% initialization
TSPtourCost  = zeros(numSteps, 1);
TSPtourCost(1) = max( eig(initCovMat) );
TSPtour_CovMat = initCovMat; 
TSPtourPos = TSPtourWaypoints(1, :);
% calculate cost
for iter = 2:numSteps
    %**************************************
    % TSP tour
    TSPtourPos = TSPtourWaypoints(iter,:);
    TSPtourNextCovMat = kalman(sys,world, TSPtourPos, TSPtour_CovMat);
    TSPtourCost(iter) = max( eig(TSPtourNextCovMat) );
    TSPtour_CovMat = TSPtourNextCovMat;
end
save(workspaceFilenameToLoad)
movefile([workspaceFilenameToLoad, '.mat'], './workspace');