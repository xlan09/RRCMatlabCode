% This is used to calculate the average cost after running greeedy and
% random search for a large number of trials

% Make sure greedyRSRH.m is run before this file

% First load workspace 
clear all;
close all;
addpath(genpath('./'))
addpath(genpath('../RRC&multRRC/RRC/fun/')) % collision, kalman
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function
workspaceFilenameToLoad = 'workspaceGreedyRSRH3000NumActions8Horizon4InitPos500and200';
load(['./workspace/', workspaceFilenameToLoad, '.mat']);

% initialization
RSsumCost = zeros(numSteps, 1);
GdySumCost = zeros(numSteps, 1);

numTrials = 100;
for trialNum = 1 : numTrials
    % init pos and init covariance matrix for random search
    randomPoint = generateRandomNode(world);
    initPos = randomPoint(1:2);
    
    RScurrentPos = initPos;
    RScurrentCovMat = initCovMat; %initCovMat is given in the loaded workspace
    
    Gdy_CrtPos = initPos;
    Gdy_CrtCovMat = initCovMat;
    
    % calculate cost
    for iter = 2:numSteps
        
        %*************************************
        % random search
        
        extention_flag = 0;
        while extention_flag==0,
            % select a random point
            randomPoint = generateRandomNode(world);
            randomPoint =randomPoint(1:2);
            
            dist = norm(RScurrentPos - randomPoint);
            if dist <= segmentLength
                new_point = randomPoint;
            else
                new_edge = (randomPoint - RScurrentPos);
                new_point = RScurrentPos + new_edge/norm(new_edge) * segmentLength;
            end
            
            if collision(new_point, RScurrentPos, world) == 0
                extention_flag = 1;
            end
        end
        
        RSnextCovMat = kalman(sys,world,new_point,RScurrentCovMat);
        RSsumCost(iter) = RSsumCost(iter) + max( eig(RSnextCovMat) );
        RScurrentPos = new_point;
        RScurrentCovMat = RSnextCovMat;
        
        %******************************************
        % Greedy algorithm
        GdyMinCost = inf;
        stuckFlag = 1;
        for i = 1:numActions
            e = [cos( (i -1) * (2*pi/numActions) ), sin( (i -1) * (2*pi/numActions) )];
            newPos = Gdy_CrtPos + e * segmentLength;
            if collision(newPos, Gdy_CrtPos, world) == 0
                GdyNextCovMat = kalman(sys,world,newPos,Gdy_CrtCovMat);
                GdyTempCost = max( eig(GdyNextCovMat) );
                
                if GdyTempCost < GdyMinCost
                    GdyMinCost = GdyTempCost;
                    nextPos = newPos;
                    nextCovMat = GdyNextCovMat;
                    stuckFlag = 0;
                end
            end
        end
        
        % if stuck, randomly choose one direction to escape
        if stuckFlag == 1
            escapeFlag = 0;
            while escapeFlag == 0
                randomAngle = rand * 2 * pi;
                unitDirect = [cos(randomAngle), sin(randomAngle)];
                newPos = Gdy_CrtPos + unitDirect * segmentLength;
                if collision(newPos, Gdy_CrtPos, world) == 0
                    escapeFlag = 1;
                    GdyNextCovMat = kalman(sys,world,newPos,Gdy_CrtCovMat);
                    GdyMinCost = max( eig(GdyNextCovMat) );
                    nextPos = newPos;
                    nextCovMat = GdyNextCovMat;
                end
            end
        end
        
        GdySumCost(iter) = GdySumCost(iter) + GdyMinCost;
        Gdy_CrtCovMat = nextCovMat;
        Gdy_CrtPos = nextPos;
        
        iter
    end
    
end

% caclulate average
RScost = RSsumCost / numTrials;
GdyCost = GdySumCost / numTrials;

% save workspace
filename = ['workspaceGreedyRSRH',num2str(numSteps),'NumActions',num2str(numActions),'Horizon',num2str(numStages)];
save(filename);
movefile([filename, '.mat'], './workspace');