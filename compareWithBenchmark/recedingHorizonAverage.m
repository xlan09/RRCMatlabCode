% This is used to calculate the average cost after running receding horizon for
% a large number of trials

% Make sure greedyRSAverage.m is run before this file

% First load workspace 
clear all;
close all;
addpath(genpath('./'))
addpath(genpath('../RRC&multRRC/RRC/fun/')) % collision, kalman
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function
workspaceFilenameToLoad = 'workspaceGreedyRSRH3000NumActions8Horizon4';
load(['./workspace/', workspaceFilenameToLoad, '.mat']);

% initialization
RHsumCost = zeros(numSteps, 1);


recedingHorizonNumTrials = 10;
for trialNum = 1 : recedingHorizonNumTrials
    % init pos and init covariance matrix for random search
    randomPoint = generateRandomNode(world);
    initPos = randomPoint(1:2);
    
    RHCrtPos = initPos;
    RHCrtCovMat = initCovMat;
    % calculate cost
    for iter = 2:numSteps
        %**********************************************
        % Receding horizon
        tic
        [action2Take, stuckFlag] = recedingHorizon(sys,world, RHCrtPos, RHCrtCovMat, numActions,numStages, segmentLength);
        toc
        % if stuck, randomly choose one direction to escape
        if stuckFlag == 1
            escapeFlag = 0;
            while escapeFlag == 0
                randomAngle = rand * 2 * pi;
                unitDirect = [cos(randomAngle), sin(randomAngle)];
                newPos = RHCrtPos + unitDirect * segmentLength;
                if collision(newPos, RHCrtPos, world) == 0
                    escapeFlag = 1;
                    RHNextCovMat = kalman(sys,world,newPos,RHCrtCovMat);
                    RHMinCost = max( eig(RHNextCovMat) );
                    nextPos = newPos;
                    nextCovMat = RHNextCovMat;
                end
            end
            
        else
            e = [cos( (action2Take(1) -1) * (2*pi/numActions) ), sin( (action2Take(1) -1) * (2*pi/numActions) )];
            newPos = RHCrtPos + e * segmentLength;
            
            nextCovMat = kalman(sys,world,newPos,RHCrtCovMat);
            RHMinCost = max( eig(nextCovMat) );
            nextPos = newPos;
        end
        
        RHsumCost(iter) = RHsumCost(iter) + RHMinCost;
        RHCrtCovMat = nextCovMat;
        RHCrtPos = nextPos;
        
        iter
    end
    
end

% caclulate average
RHCost = RHsumCost / recedingHorizonNumTrials;

filename = ['workspaceGreedyRSRH',num2str(numSteps),'NumActions',num2str(numActions),'Horizon',num2str(numStages)];
save(filename);
movefile([filename, '.mat'], './workspace');