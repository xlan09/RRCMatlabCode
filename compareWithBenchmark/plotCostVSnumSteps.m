clear all;
close all;
clc;
addpath(genpath('./'))
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function
%*******************************************************************************
% load workspaceGreedyRSRH3000NumActions8Horizon3InitPos400and600.mat
% load workspaceGreedyRSRH3000NumActions8Horizon3InitPos500and200.mat

load workspaceGreedyRSRH3000NumActions8Horizon4InitPos400and600.mat
% load workspaceGreedyRSRH3000NumActions8Horizon4InitPos500and200.mat
% load workspaceGreedyRSRH3000NumActions8Horizon4InitPos800and600.mat

% load workspaceGreedyRSRH1000NumActions8Horizon5InitPos800and600.mat
% load workspaceGreedyRSRH1000NumActions8Horizon5InitPos500and200.mat
%*******************************************************************************
% plot
handles = [];
figure(1);
handles = [handles; gcf];
clf;
% axis([0 3000 0 150])
hold on;
grid on;
linewidth = 1;

xlabel('Number of Steps')
ylabel('Largest Eigenvalue of Covariance Matrix')
title('Cost VS. Number of Steps')
plot(1:numSteps,RRCcost,'b', 'linewidth',linewidth);
plot(1:numSteps,RRCstarCost,'r', 'linewidth', linewidth);
plot(1:numSteps, TSPtourCost, 'k', 'linewidth', linewidth);

plot(1:numSteps,RScost,'c', 'linewidth', linewidth);
plot(1:numSteps,GdyCost,'m', 'linewidth', linewidth);
plot(1:numSteps,RHCost,'g', 'linewidth', linewidth);
legend('RRC','RRCstar', 'TSP tour', 'Random Search','Greedy','Receding Horizon', 'Location', 'northwest')
% save fig
figName=['costCompareInitPos',num2str(initPos(1)),'and',num2str(initPos(2)),'Horizon',num2str(numStages)];

% xlabel('Number of Steps')
% ylabel('Log10 of Largest Eigenvalue of Covariance Matrix')
% title('Log10(Cost) VS. Number of Steps')
% plot(1:numSteps,log10(RRCcost),'b', 'linewidth',linewidth);
% plot(1:numSteps,log10(RRCstarCost),'r', 'linewidth', linewidth);
% plot(1:numSteps,log10(RScost),'c', 'linewidth', linewidth);
% plot(1:numSteps,log10(GdyCost),'m', 'linewidth', linewidth);
% plot(1:numSteps,log10(RHCost),'g', 'linewidth', linewidth);
% legend('RRC','RRCstar', 'Random Search','Greedy','Receding Horizon','Location', 'northwest')
% % save fig
% figName=['logCostCompareInitPos',num2str(initPos(1)),'and',num2str(initPos(2)),'Horizon',num2str(numStages)];

set(gcf, 'PaperPosition', [-0.1 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

%*******************************************************************************
% plot compare between TSPtour RRC and RRCstar
figure;
handles = [handles; gcf];
clf;

hold on;
grid on;
linewidth = 1;

xlabel('Number of Steps')
ylabel('Largest Eigenvalue of Covariance Matrix')
title('Cost VS. Number of Steps')
plot(1:numSteps,RRCcost,'b', 'linewidth',linewidth);
plot(1:numSteps,RRCstarCost,'r', 'linewidth', linewidth);
plot(1:numSteps, TSPtourCost, 'k', 'linewidth', linewidth);
legend('RRC','RRCstar', 'TSP tour', 'Location', 'northwest')

% save fig
figName=['costCompareRRC_RRCstar_TSPtourInitPos',num2str(initPos(1)),'and',num2str(initPos(2)),'Horizon',num2str(numStages)];
set(gcf, 'PaperPosition', [-0.1 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

%*******************************************************************************
% draw trajectory
% Random search
showOcean(world.environment, 1, 'imagesc');
set(gcf,'Name',['Random search algorithm starting at [', num2str(initPos(1)),', ', num2str(initPos(2)), ']']);
% plot points of interest
for i=1:world.num_points
    plot(world.px(i), world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(world.px(i)+10,world.py(i)+10,num2str(i),'color','m','fontsize',14,'fontweight','bold');
end
plot(RSwaypoints(:,1),RSwaypoints(:,2),'b','linewidth',1.2)
handles = [handles; gcf];
figName = ['RStraj', num2str(initPos(1)),'and',num2str(initPos(2))];
set(gca, 'LooseInset', [0 0 0 0]);
print([figName,'.png'],'-dpng','-r400');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');

% ******************************************************************************
% greedy
showOcean(world.environment, 1, 'imagesc');
set(gcf,'Name',['Greedy algorithm starting at [', num2str(initPos(1)),', ', num2str(initPos(2)), ']']);
% plot points of interest
for i=1:world.num_points
    plot(world.px(i), world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(world.px(i)+10,world.py(i)+10,num2str(i),'color','m','fontsize',14,'fontweight','bold');
end
plot(GdyWaypoints(:,1),GdyWaypoints(:,2),'b-o','linewidth',1.2)
% [periodIndex, findFlag] = findPeriodicTraj(GdyWaypoints);
% % converge to periodic traj ?
% if findFlag == 1
%     plot(GdyWaypoints(periodIndex(1): periodIndex(2), 1),GdyWaypoints(periodIndex(1): periodIndex(2), 2),'r-o','linewidth',2)
% end
handles = [handles; gcf];
figName = ['greedyTraj', num2str(initPos(1)),'and',num2str(initPos(2))];
set(gca, 'LooseInset', [0 0 0 0]);
print([figName,'.png'],'-dpng','-r400');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');

% ******************************************************************************
% receding horizon
showOcean(world.environment, 1, 'imagesc');
set(gcf,'Name',['Receding horizon starting at [', num2str(initPos(1)),', ', num2str(initPos(2)), ']']);
% plot points of interest
for i=1:world.num_points
    plot(world.px(i), world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(world.px(i)+10,world.py(i)+10,num2str(i),'color','m','fontsize',14,'fontweight','bold');
end
plot(RHWaypoints(:,1),RHWaypoints(:,2),'b-o','linewidth',1.2)
% [periodIndex, findFlag] = findPeriodicTraj(RHWaypoints);
% % converge to periodic traj ?
% if findFlag == 1
%     plot(RHWaypoints(periodIndex(1): periodIndex(2), 1),RHWaypoints(periodIndex(1): periodIndex(2), 2),'r-o','linewidth',2)
% end
handles = [handles; gcf];
figName = ['RHtraj', num2str(initPos(1)),'and',num2str(initPos(2)),'Horizon',num2str(numStages)];
set(gca, 'LooseInset', [0 0 0 0]);
print([figName,'.png'],'-dpng','-r400');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');

% tile figs
tilefigs(handles);