clear VARIABLES;
clear GLOBAL;
close all;
clc;
global tree;
global world;
global logHandle;

logHandle = fopen(['./log/', date, '.txt'], 'a+');
fprintf(logHandle, '\n***************************************************\n');
fprintf(logHandle, 'Starting at %s\n', datestr(clock));
fprintf(logHandle, '***************************************************\n');

addpath(genpath('./'))
addpath(genpath('../learnOceanTemp/fun')) % showOcean function
addpath(genpath('../learnOceanTemp/workspace')) % showOcean function

seed = 5000;
str = RandStream.create('mt19937ar','seed',seed);
RandStream.setGlobalStream(str);
%***********************************
% parameters
segmentLength = 50; % Length between any two nodes.
numSteps = 5000; % num of iterations for RRTstar

%*******************************************************************************
% Load environment data
environmentData = load('basisCenterLonI1200LatJ800Cluster9Wtemp0Wdist1.mat','environment','centroid');
environment = environmentData.environment;
centroid = environmentData.centroid;

% create the environment which needs to be monitored
world= createWorld(environment, centroid);

% visit all the POI
TSPtour = [];
% plot the environment and the path found by RRTstar
TSPtourHandle = figure('name', 'TSP Tour by RRT*');
clf;
plotWorld;

visitOrder = [1; 6; 7; 4; 3; 8; 5; 2; 9];
pointsToVisit = centroid(visitOrder, :);
numPointsToVisit = size(pointsToVisit, 1);
for i = 1 : numPointsToVisit
    currPointIndex = i;
    nextPointIndex = mod(i, numPointsToVisit) + 1;
    % start node and end node
    startNode=generateRandomNode;
    startNode.pos = pointsToVisit(currPointIndex, :);
    endNode = generateRandomNode;
    endNode.pos = pointsToVisit(nextPointIndex, :);
    endNode.distToRoot = inf; %dist between start node and end node is inf
    
    tree = repmat(startNode, numSteps, 1);
    currTreeSize = 1;
    
    % RRT star num of iterations
    for currIterNum = 1:numSteps - 1
        currIterNum
        [currTreeSize, endNode] = RRTStarExtendTree(currTreeSize, segmentLength, endNode);
    end
    figure(TSPtourHandle);
    RRTstarPath = plotPaths(currTreeSize, endNode);
    TSPtour = [RRTstarPath(1:end-1, :), TSPtour];
end
figure(TSPtourHandle)
plotPOI;
plot([TSPtour(:, 1); TSPtour(1, 1)], [TSPtour(:, 2); TSPtour(1, 2)], 'r', 'linewidth', 2);

% Save figure
figName = ['TSPtourByRRTstarNumIter', num2str(numSteps),'RRTsegment',num2str(segmentLength),'Seed',num2str(seed)];
set(gca, 'LooseInset', [0 0 0 0]);
print([figName,'.png'],'-dpng','-r400');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');

% save workspace
filename=['TSPtourNumIter', num2str(numSteps),'RRTsegment',num2str(segmentLength),'Seed',num2str(seed)];
workspaceName=['workspace',filename];
save(workspaceName);
movefile([workspaceName,'.mat'],'./workspace');