clear all;
close all;
clc;
addpath(genpath('./'))
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function


load workspaceGreedyRSRH3000NumActions8Horizon4

% plot
handles = [];
figure(1);
handles = [handles; gcf];
clf;
% axis([0 3000 0 150])
hold on;
grid on;
linewidth = 1;

xlabel('Number of Steps')
ylabel('Largest Eigenvalue of Covariance Matrix')
title('Cost VS. Number of Steps')
plot(1:numSteps,RRCcost,'b', 'linewidth',linewidth);
plot(1:numSteps,RRCstarCost,'r', 'linewidth', linewidth);
plot(1:numSteps, TSPtourCost, 'k', 'linewidth', linewidth);

plot(1:numSteps,RScost,'c', 'linewidth', linewidth);
plot(1:numSteps,GdyCost,'m', 'linewidth', linewidth);
plot(1:numSteps,RHCost,'g', 'linewidth', linewidth);
legend('RRC','RRCstar', 'TSP tour', 'Random Search','Greedy','Receding Horizon', 'Location', 'northwest')
% save fig
figName=['averageCostCompareHorizon',num2str(numStages)];

% xlabel('Number of Steps')
% ylabel('Log10 of Largest Eigenvalue of Covariance Matrix')
% title('Log10(Cost) VS. Number of Steps')
% plot(1:numSteps,log10(RRCcost),'b', 'linewidth',linewidth);
% plot(1:numSteps,log10(RRCstarCost),'r', 'linewidth', linewidth);
% plot(1:numSteps,log10(RScost),'c', 'linewidth', linewidth);
% plot(1:numSteps,log10(GdyCost),'m', 'linewidth', linewidth);
% plot(1:numSteps,log10(RHCost),'g', 'linewidth', linewidth);
% legend('RRC','RRCstar', 'Random Search','Greedy','Receding Horizon','Location', 'northwest')
% % save fig
% figName=['logCostCompareInitPos',num2str(initPos(1)),'and',num2str(initPos(2)),'Horizon',num2str(numStages)];

set(gcf, 'PaperPosition', [-0.1 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');