function [action2Take, stuckFlag] = recedingHorizon(sys,world, crtPos, crtCovMat, numActions,numStages,segmentLength)
minCost = inf;
stuckFlag = 1;
action2Take = [];

actionList = enumerate(numActions, numStages);% this is a numActions^numStages by numStages matrix
for i = 1:size(actionList,1)
    action = actionList(i,:);
    startPos = crtPos;
    startCovMat = crtCovMat;
    % consider one action
    actionFeasibleFlag = 1;
    for j = 1:numStages
        e = [cos( (action(j) -1) * (2*pi/numActions) ), sin( (action(j) -1) * (2*pi/numActions) )];
        newPos = startPos + e * segmentLength;
        if collision(newPos, startPos, world) == 0
            nextCovMat = kalman(sys,world,newPos,startCovMat);
            startCovMat = nextCovMat;
            startPos = newPos;
        else
            actionFeasibleFlag = 0;
            break; % break the inner for loop
        end
    end
    
    if actionFeasibleFlag == 1
        tempCost = max( eig(nextCovMat) );
        if tempCost < minCost
            minCost = tempCost;
            action2Take = action;
            stuckFlag = 0;
        end
    end
end