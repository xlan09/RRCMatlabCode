function RRTstarPath = plotPaths(currTreeSize, endNode)
global tree;

% plot random tree
leafNodes = repmat(tree(1), currTreeSize, 1);
numLeaf = 0;
for i=1:currTreeSize
    if tree(i).isLeaf == 1
        numLeaf = numLeaf + 1;
        leafNodes(numLeaf) = tree(i);
    end
end

%find all the paths inside the tree
for i = 1:numLeaf
    parentId = leafNodes(i).parentId;
    path = leafNodes(i).pos;
    while parentId >= 1
        path = [path;tree(parentId).pos];
        parentId = tree(parentId).parentId;
    end
    plot(path(:, 1), path(:, 2), 'color', [100/255 149/255 237/255]);
end

% plot the path returned by RRTstar
RRTstarPath = endNode.pos;
parentId = endNode.parentId;
while parentId >= 1
    RRTstarPath = [RRTstarPath; tree(parentId).pos];
    parentId = tree(parentId).parentId;
end
plot(RRTstarPath(:, 1), RRTstarPath(:, 2), 'r', 'linewidth', 2);