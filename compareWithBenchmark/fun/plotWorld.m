function plotWorld()
global world;

% plot environment
axis equal;
hold on;

environment = world.environment;
[numRow,numCol] = size(environment);
xMax = numRow;
yMax = numCol;
axis([0 xMax 0 yMax]);
colormap(hot);
imagesc(environment');