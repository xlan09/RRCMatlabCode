function [periodIndex, findFlag] = findPeriodicTraj(waypoints)
% find whether there exists a periodic trajectory in the waypoints
% wayponts is a numSteps by 2 matrix
% Return:
% periodIndex is the index in the waypoints where the periodicity starts and
% ends, if there is no periodic traj in waypoints, findFlag = 0.

% make sure waypoints is a numSteps by 2 matrix
[numRow, numCol] = size(waypoints);
if numRow < numCol
    waypoints = waypoints';
end

findFlag = 0;
tol = 10^(-10);
numSteps = size(waypoints, 1);
for i = numSteps-1 : -1 : 1
   if norm(waypoints(i,:) - waypoints(end,:)) < tol
       periodIndex = [i, numSteps];
       findFlag = 1;
       break;
   end
end

if findFlag == 0
    periodIndex = [1, numSteps];
end