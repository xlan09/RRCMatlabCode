function collision_flag = collision(node1, node2)
% collision check to see if a path from node1 to node2 is in
% collsion with obstacles
%
%collision_flag=1 means collision happens and the path is NOT feasible;
%otherwise, collision_flag=0;

global world;

%check whether generated random node is outside of the region
% node position
nodePos1=node1.pos;
nodePos2=node2.pos;
environment = world.environment;
SWcorner = world.SWcorner;
NEcorner = world.NEcorner;

NWcorner = [SWcorner(1) - abs( NEcorner(1) - SWcorner(1) );
            NEcorner(2) - abs( NEcorner(2) - SWcorner(2) )];
SEcorner = [NWcorner(1) + abs( NEcorner(1) - SWcorner(1) );
            NWcorner(2) + abs( NEcorner(2) - SWcorner(2) )];
if nodePos1(1) < NWcorner(1) || nodePos1(1) > SEcorner(1) || ...
   nodePos1(2) < NWcorner(2) || nodePos1(2) > SEcorner(2) || ...
   nodePos2(1) < NWcorner(1) || nodePos2(1) > SEcorner(1) || ...
   nodePos2(2) < NWcorner(2) || nodePos2(2) > SEcorner(2)
    collision_flag = 1;
else
    xInterval = 1;
    yInterval = 1;
    num_xInterval = ceil( abs(nodePos1(1) - nodePos2(1)) / xInterval );
    num_yInterval = ceil( abs(nodePos1(2) - nodePos2(2)) / yInterval );
    numPointsBetwn = max(num_xInterval + 1, num_yInterval + 1);
    alpha = linspace(0,1, numPointsBetwn);
    pointsBetwn = ceil( [alpha * nodePos1(1) + (1-alpha)* nodePos2(1);...
        alpha * nodePos1(2) + (1-alpha)* nodePos2(2)] );% 2 by numPointsBetwn matrix
    % check collision
    collision_flag = 0;
    for i = 1:numPointsBetwn
        % In the environment matrix, if one element = -1, then it is an
        % obstacle
        % note pointsBetwn(2,i) corresponds to the first dim of environment, pointsBetwn(1,i)
        % corresponds to the second dim of environment
        if environment(pointsBetwn(1,i), pointsBetwn(2,i)) == -1
            collision_flag = 1;
            break;
        end
    end
end
