function node = generateRandomNode()
global world;

% create a random node in the free space(initialize)
NEcorner = world.NEcorner;
SWcorner = world.SWcorner;
NWcorner = [SWcorner(1) - abs( NEcorner(1) - SWcorner(1) );
            NEcorner(2) - abs( NEcorner(2) - SWcorner(2) )];

% randomly pick configuration
posX       = NWcorner(1) + abs(NEcorner(1) - SWcorner(1)) * rand;% x coordinate of the random node in the 2-D plane
posY       = NWcorner(2) + abs(NEcorner(2) - SWcorner(2)) * rand;% y coordinate of the random node in the 2-D plane
node.pos = [posX, posY];
node.isLeaf = 1;
node.parentId = 0;
node.distToRoot = 0;

% check collision with obstacle
while collision(node, node),
    posX       = NWcorner(1) + abs(NEcorner(1) - SWcorner(1)) * rand;
    posY       = NWcorner(2) + abs(NEcorner(2) - SWcorner(2)) * rand;
    node.pos = [posX, posY];
    node.isLeaf = 1;
    node.parentId = 0;
    node.distToRoot = 0;
end
