function [currTreeSize, endNode] = RRTStarExtendTree(currTreeSize, segmentLength, endNode)
global tree;
global world;
global logHandle;

%% Extend tree to random node
%volume of the free space
volume=abs(world.NEcorner(1)-world.SWcorner(1)) * abs(world.NEcorner(2)-world.SWcorner(2));
gamaRRT=2*(1+1/2)^(1/2)*(volume/pi)^(1/2);

%extention_flag used to denote whether we extend the tree successfully. If
%NOT, we need to generate random node again until we extend it successfully
extention_flag = 0;
while extention_flag == 0,
    % select a random point
    randomNode = generateRandomNode;
    
    % find leaf inside the tree that is closest to the randomPoint
    distToAllVertex = distToVertexInTree(randomNode, currTreeSize);
    [dist, nearestNodeId] = min(distToAllVertex);
    if dist >= segmentLength
        %if the distance between the randomPoint and the nearest vertex
        %inside the tree is smaller than "segmentLength", then we just
        %choose this randomPoint as the node to add to the tree; Otherwise,
        %we can only extend the tree towards this randomPoint by a maximum
        %distance given by "segmentLength"
        directionVec = randomNode.pos-tree(nearestNodeId).pos;
        randomNode.pos = tree(nearestNodeId).pos + directionVec/norm(directionVec) * segmentLength;
    end
    
    %% find which vertex we should connect this new_node to.    
    if collision(randomNode, tree(nearestNodeId))==0
        %radius of the RRT*
        radius=min(gamaRRT*(log(currTreeSize)/currTreeSize)^(1/2), segmentLength);
        distToAllVertex = distToVertexInTree(randomNode, currTreeSize);
                
        nearVertexList=[];%this is used to store all the index of the vertices
        %inside the ball centered at the new node with the above radius
        for i=1:length(distToAllVertex)
            if distToAllVertex(i) <= radius
                nearVertexList = [nearVertexList;i];
            end
        end
        numNearVertex = length(nearVertexList);  
        
        % add the potential nodes to the tree
        tree(currTreeSize + 1) = randomNode;
        currTreeSize = currTreeSize + 1;
        
        % compare cost
        min_id=nearestNodeId; %assume that the nearest vertex has the minimum cost
        minPathCost = distToRoot(tree(nearestNodeId)) + norm(randomNode.pos - tree(nearestNodeId).pos);
        
        if numNearVertex > 1
            %if there are at least two vertices inside the ball
            for i=1:numNearVertex
                if collision(randomNode,tree(nearVertexList(i)))==0 
                    pathCost = distToRoot(tree(nearVertexList(i))) + norm(randomNode.pos - tree(nearVertexList(i)).pos);
                    if  pathCost < minPathCost
                        minPathCost = pathCost;
                        min_id = nearVertexList(i);
                    end
                end
            end
        end
        tree(min_id).isLeaf = 0; %mark it is no longer a leaf
        tree(currTreeSize).parentId = min_id; %set new_node's parent
        extention_flag=1; %it is used to denote we have extended the tree
        
        %rewire
        for i=1:numNearVertex
            if (nearVertexList(i) ~= min_id && collision(randomNode, tree(nearVertexList(i))) ==0)
                pathCost = distToRoot(tree(currTreeSize)) + norm(randomNode.pos - tree(nearVertexList(i)).pos);
                if  pathCost < distToRoot(tree(nearVertexList(i)))
                    tree(currTreeSize).isLeaf = 0;
                    tree(nearVertexList(i)).parentId = currTreeSize; %make new node as the parent of the near node
                end
            end
        end
    end
end

% check whether find a path to endNode
if norm(endNode.pos - tree(currTreeSize).pos) <= segmentLength 
    pathToEndNodeCost = distToRoot(tree(currTreeSize)) + norm(endNode.pos - tree(currTreeSize).pos);
    if pathToEndNodeCost < endNode.distToRoot
        fprintf(logHandle, 'New lower cost path between startNode [%.2f, %.2f] and endNode [%.2f, %.2f] is found! \n', tree(1).pos(1), tree(1).pos(2), endNode.pos(1), endNode.pos(2));
        fprintf(logHandle, 'Old cost is %f and new lower cost is %f! \n', endNode.distToRoot, pathToEndNodeCost);
        endNode.parentId = currTreeSize; % Does not add endNode to tree, just update its parent and dist to root
        endNode.distToRoot = pathToEndNodeCost;
    end
end

  
function distToAllVertex = distToVertexInTree(randomNode, currTreeSize)
%% find the dist from randomNode to vertices in the tree
global tree;

distToAllVertex = [];
points=[tree(1:currTreeSize).pos];
if currTreeSize >= 1
    pointsDim=length(tree(1).pos);
    points=reshape(points,pointsDim,currTreeSize);
    % distance between the randomPos and all the vertices inside the tree
    distToAllVertex = distfun(randomNode.pos, points' );
end



function out = distfun(center, data)
%% calculates the Euclidean distance
%	between each row in CENTER and each row in DATA, and returns a
%	distance matrix OUT of size M by N, where M and N are row
%	dimensions of CENTER and DATA, respectively, and OUT(I, J) is
%	the distance between CENTER(I,:) and DATA(J,:).
%
% center: M by n matrix, n is the dimension of the data. E.g. if n=2, means
% center(i,:) is one point in the plane.
% data: N by n matrix
out = zeros(size(center, 1), size(data, 1));

% fill the output matrix
if size(center, 2) > 1,
    for k = 1:size(center, 1),
        out(k, :) = sqrt(sum(((data-ones(size(data, 1), 1)*center(k, :)).^2)'));
    end
else	% 1-D data
    for k = 1:size(center, 1),
        out(k, :) = abs(center(k)-data)';
    end
end


function dist = distToRoot(node)
global tree;

dist = 0;
currNode = node;
parentId = currNode.parentId;
while parentId > 0
    dist = dist + norm(currNode.pos - tree(parentId).pos);
    currNode = tree(parentId);
    parentId = tree(parentId).parentId;
end





