function plotPOI()
global world;
% plot points of interest
for i=1:world.num_points
    plot(world.px(i), world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(world.px(i)+10,world.py(i)+10,num2str(i),'color','m','fontsize',14,'fontweight','bold');
end