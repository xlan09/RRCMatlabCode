clear all;
close all;
clc;
tic;
% ******************************************************************************
addpath(genpath('./'))
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function
addpath(genpath('../RRC&multRRC/RRC/fun/')) % kalman
addpath(genpath('../compareWithBenchmark/workspace'))
%*******************************************************************************
% load workspaceGreedyRSRH3000NumActions8Horizon3InitPos400and600.mat
% load workspaceGreedyRSRH3000NumActions8Horizon3InitPos500and200.mat
load workspaceGreedyRSRH3000NumActions8Horizon4InitPos500and200.mat
% load workspaceGreedyRSRH3000NumActions8Horizon4InitPos400and600.mat
%*******************************************************************************
% make sure that the following two parameters are the same with the
% parameters in the kalman function.
sensingScale = 10;
sensingRadius  = 100;

% Animation parameter
aniStartIter = 439; % must greater than or equal to 1
aniEndIter = 440; % must be smaller than or equal to 'numSteps'
%*******************************************************************************
% Calculate the field
n = world.num_points;

% initialization
RRCSIGMA = zeros(n,n,numSteps);
RRCSIGMA(:,:,1) = initCovMat;
RRCstarSIGMA = zeros(n,n,numSteps);
RRCstarSIGMA(:,:,1) = initCovMat;
RSSIGMA = zeros(n,n,numSteps);
RSSIGMA(:,:,1) = initCovMat;
greedySIGMA = zeros(n,n,numSteps);
greedySIGMA(:,:,1) = initCovMat;
RHSIGMA = zeros(n,n,numSteps);
RHSIGMA(:,:,1) = initCovMat;

for i = 2:numSteps 
    RRCSIGMA(:,:,i) = kalman(sys,world, RRCwaypoints(i,:), RRCSIGMA(:,:,i-1));
%     RRCstarSIGMA(:,:,i) = kalman(sys,world, RRCstarWaypoints(i,:), RRCstarSIGMA(:,:,i-1));
end


% plot the field values in an image with hot color standing for higher value
xMin = 0;
xMax = size(world.environment, 1);
yMin = 0;
yMax = size(world.environment, 2);

% points in the environment
px = 0:xMax;
py = 0:yMax;
xNum = length(px);
yNum = length(py);

% coefficient matrix
% coefficients(:,:,i) is i-th coefficient for all the points
coefficients = zeros(xNum,yNum,n);
[PY,PX]=meshgrid(py,px);

% Calculate coefficients of the linear combination of the basis function
qx=world.px;
qy=world.py;
for i=1:n
   coefficients(:,:,i)=sensingScale * exp((-1)*((PX-qx(i)).^2 + (PY-qy(i)).^2)/(2*sensingRadius^2));
end

% Calculate the estimation uncertainty of the field at each point
% in the environment, which is equal to tr(M(p)*Sigma)
RRCFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCSIGMA(:,:,aniStartIter) );
% RRCstarFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCstarSIGMA(:,:,aniStartIter) );

%% *****************************************************************************
% % plot contour
% mainFigHandle = figure('units','normalized','outerposition',[0 0 1 1]);
% clf;
% axis equal;
% hold on;

% find obstacles
islands = findIslands(world.environment);

% ********************
RRCplot = figure;
axis equal;
hold on;
axis([xMin xMax yMin yMax]);
% RRCtext = text(100, -100, ['(d) RRC trajectory, \rho(\Sigma) = ', num2str(max(eig(RRCSIGMA(:,:,aniStartIter))))], 'fontSize', 20);
% text(300, -200, 'Animation of how estimation uncertainties change when sensing robot moves to take measurements.', 'fontSize', 20)
% plot uncertainty field
RRCFH = plotField(PX, PY, RRCFU, min( min(RRCFU) ), max( min(RRCFU) ));
hcb = colorbar;
Ylim=get(hcb,'YLim');% min y and max y for the bar
set(hcb,'YTick',Ylim,'YTickLabel',{'Low', 'High'},'fontSize',20, 'fontWeight','bold'); 
% set(hcb,'YTickMode','manual', 'position',[0.43, 0.1, 0.08, 0.42])
% plot obstacle
plotIslands(islands);

% % ********************
% RRCstarPlot = subplot(2,3,5);
% axis equal;
% hold on;
% axis([xMin xMax yMin yMax]);
% set(gca,'Position', [0.57 0.02 0.3 0.6])
% RRCstarText = text(100, -100, ['(e) RRC* trajectory,  \rho(\Sigma) = ', num2str(max(eig(RRCstarSIGMA(:,:,aniStartIter))))],'fontSize', 20);
% 
% % plot uncertainty field
% RRCstarFH = plotField(PX, PY, RRCstarFU, minField, maxField);
% % plot obstacle
% plotIslands(islands);

% ******************************************************************************
% plot the trajectory from 1 to aniStartIter
% axes(RSplot);
% RSFP = plot(RSwaypoints(1:aniStartIter, 1),RSwaypoints(1:aniStartIter, 2),'r','linewidth',2);
% axes(greedyPlot);
% greedyFP = plot(GdyWaypoints(1:aniStartIter, 1),GdyWaypoints(1:aniStartIter, 2),'r','linewidth',2);
% axes(RHplot);
% RHFP = plot(RHWaypoints(1:aniStartIter, 1),RHWaypoints(1:aniStartIter, 2),'r','linewidth',2);
RRCFP = plot(RRCwaypoints(1:aniStartIter, 1),RRCwaypoints(1:aniStartIter, 2),'r','linewidth',2);
% axes(RRCstarPlot);
% RRCstarFP = plot(RRCstarWaypoints(1:aniStartIter, 1),RRCstarWaypoints(1:aniStartIter, 2),'r','linewidth',2);

% draw robot
radius = 30;
omega=0:.01:2*pi;
robotX=radius*cos(omega);
robotY=radius*sin(omega);

% pos = RSwaypoints(aniStartIter,:);
% RSrobot = drawRobot(pos, radius, RSplot);
% pos = GdyWaypoints(aniStartIter,:);
% greedyRobot = drawRobot(pos, radius, greedyPlot);
% pos = RHWaypoints(aniStartIter,:);
% RHrobot = drawRobot(pos, radius, RHplot);
pos = RRCwaypoints(aniStartIter,:);
RRCrobot = drawRobot(pos, radius, gca);
% pos = RRCstarWaypoints(aniStartIter,:);
% RRCstarRobot = drawRobot(pos, radius, RRCstarPlot);

% Create an animation with 'avi' format 
fileName2Save = ['animationRobotMoveAlongRRCTraj', num2str(aniStartIter), 'to', num2str(aniEndIter)];
vidFilename = [fileName2Save,'.avi'];
vidObj = VideoWriter(vidFilename);
vidObj.FrameRate = 2;
open(vidObj);
currFrame = getframe(gcf);
writeVideo(vidObj,currFrame);

% snapShotNum=4;
% instants=linspace(1,pathLength,snapShotNum);
saveInstants=[1 50 100 200 400];
% minRSFU = min( min(RSFU) );
% maxRSFU = max( max(RSFU) );
% minGreedyFU = min( min(greedyFU) );
% maxGreedyFU = max( max(greedyFU) );
% minRHFU = min( min(RHFU) );
% maxRHFU = max( max(RHFU) );
minRRCFU = min( min(RRCFU) );
maxRRCFU = max( max(RRCFU) );
% minRRCstarFU = min( min(RRCstarFU) );
% maxRRCstarFU = max( max(RRCstarFU) );

% start animation
for i = aniStartIter:aniEndIter
%         axes(RSplot);
%         set(RSFP, 'XData', RSwaypoints(1:i, 1), 'YData', RSwaypoints(1:i, 2) );
% %         plot(RSwaypoints(i-1:i, 1),RSwaypoints(i-1:i, 2),'r','linewidth',2);
%         set(RSrobot,'XData',RSwaypoints(i,1)+robotX,'YData',RSwaypoints(i,2)+robotY);
%         RSFU = uncertaintyMex(coefficients, xNum, yNum, n, RSSIGMA(:,:,i) );
%         set(RSFH,'ZData',RSFU);
%         minRSFU = min(minRSFU, min( min(RSFU) ));
%         maxRSFU = max(maxRSFU, max( max(RSFU) ));
%         set(RStext, 'string', ['(a) Random serach trajectory, \rho(\Sigma) = ', num2str(max(eig(RSSIGMA(:,:,i))))] );
%         
%         axes(greedyPlot);
%         set(greedyFP, 'XData', GdyWaypoints(1:i, 1), 'YData', GdyWaypoints(1:i, 2) );
% %         plot(GdyWaypoints(i-1:i, 1),GdyWaypoints(i-1:i, 2),'r','linewidth',2);
%         set(greedyRobot,'XData',GdyWaypoints(i,1)+robotX,'YData',GdyWaypoints(i,2)+robotY);
%         greedyFU = uncertaintyMex(coefficients, xNum, yNum, n, greedySIGMA(:,:,i) );
%         set(greedyFH,'ZData',greedyFU);
%         minGreedyFU = min(minGreedyFU, min( min(greedyFU) ));
%         maxGreedyFU = max(maxGreedyFU, max( max(greedyFU) ));
%         set(greedyText, 'string', ['(b) Greedy trajectory, \rho(\Sigma) = ', num2str(max(eig(greedySIGMA(:,:,i))))] );
%         
%         axes(RHplot);
%         set(RHFP, 'XData', RHWaypoints(1:i, 1), 'YData', RHWaypoints(1:i, 2) );
% %         plot(RHWaypoints(i-1:i, 1),RHWaypoints(i-1:i, 2),'r','linewidth',2);
%         set(RHrobot,'XData',RHWaypoints(i,1)+robotX,'YData',RHWaypoints(i,2)+robotY);
%         RHFU = uncertaintyMex(coefficients, xNum, yNum, n, RHSIGMA(:,:,i) );
%         set(RHFH,'ZData',RHFU);
%         minRHFU = min(minRHFU, min( min(RHFU) ));
%         maxRHFU = max(maxRHFU, max( max(RHFU) ));
%         set(RHtext, 'string', ['(c) Receding horizon trajectory, \rho(\Sigma) = ', num2str(max(eig(RHSIGMA(:,:,i))))] );
%         
        axes(gca);
        set(RRCFP, 'XData', RRCwaypoints(1:i, 1), 'YData', RRCwaypoints(1:i, 2) );
%         plot(RRCwaypoints(i-1:i, 1),RRCwaypoints(i-1:i, 2),'r','linewidth',2);
        set(RRCrobot,'XData',RRCwaypoints(i,1)+robotX,'YData',RRCwaypoints(i,2)+robotY);
        RRCFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCSIGMA(:,:,i) );
        set(RRCFH,'ZData',RRCFU);
        minRRCFU = min(minRRCFU, min( min(RRCFU) ));
        maxRRCFU = max(maxRRCFU, max( max(RRCFU) ));
        set(RRCtext, 'string', ['(d) RRC trajectory, \rho(\Sigma) = ', num2str(max(eig(RRCSIGMA(:,:,i))))] );
        
%         axes(RRCstarPlot);
%         set(RRCstarFP, 'XData', RRCstarWaypoints(1:i, 1), 'YData', RRCstarWaypoints(1:i, 2) );
% %         plot(RRCstarWaypoints(i-1:i, 1),RRCstarWaypoints(i-1:i, 2),'r','linewidth',2);
%         set(RRCstarRobot,'XData',RRCstarWaypoints(i,1)+robotX,'YData',RRCstarWaypoints(i,2)+robotY);
%         RRCstarFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCstarSIGMA(:,:,i) );
%         set(RRCstarFH,'ZData',RRCstarFU);
%         minRRCstarFU = min(minRRCstarFU, min( min(RRCstarFU) ));
%         maxRRCstarFU = max(maxRRCstarFU, max( max(RRCstarFU) ));
%         set(RRCstarText, 'string', ['(e) RRC* trajectory,  \rho(\Sigma) = ', num2str(max(eig(RRCstarSIGMA(:,:,i))))] );
%         
% %         title(['Time t=',num2str(instants(i)),'s'],'FontSize',14);
%         pause(0.001)
        % add frame
        currFrame = getframe(gcf);
        writeVideo(vidObj,currFrame);

%       for j=1:length(saveInstants)
%          if i==saveInstants(j)
%              % Save figure
%              set(gcf, 'PaperPosition', [-0.48 -0.2 6.0 4.5]); %Position plot at left hand corner with width 5 and height 5.
%              set(gcf, 'PaperSize', [5.5 4.3]); %Set the paper to have width 5 and height 5.
%              saveas(gcf, ['FieldChange4BasisFun',int2str(j)], 'pdf') %Save figure
%              
%          end
%       end
    i
end
runningTime = toc;
% workspaceName = fileName2Save;
% save(workspaceName);
% movefile([workspaceName, '.mat'], './workspace');
% close(vidObj);
% movefile(vidFilename,'/home/lan/Videos/fieldChangComparison/')