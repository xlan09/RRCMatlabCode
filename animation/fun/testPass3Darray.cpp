#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
#define threeDarray_IN prhs[0]
#define numRow_IN prhs[1]
#define numCol_IN prhs[2]
#define num3Ddim_IN prhs[3]

    double *threeDarray = mxGetPr(threeDarray_IN);
    int numRow = (int) mxGetScalar(numRow_IN);
    int numCol = (int) mxGetScalar(numCol_IN);
    int num3Ddim = (int) mxGetScalar(num3Ddim_IN);

    // note that in MATLAB, array is stored columnwise, while in normal C++,
    // array is stored rowwise.
    for (int i = 0; i < numRow; ++i) {
        for (int j = 0; j < numCol; ++j) {
            for (int k = 0; k < num3Ddim; ++k) {
                mexPrintf("%f  ", threeDarray[k * (numRow * numCol) + j * numRow + i]);
            }
            mexPrintf("\n");
        }
        mexPrintf("\n");
    }
}
