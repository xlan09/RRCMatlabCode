function plotIslands(islands)
% [numRow, numCol] = size(environment);
% for i = 1:numRow
%         for j = 1:numCol
%             if environment(i,j) == -1 || isnan(environment(i,j))
%                 patch([i-1, i, i, i-1],...
%                     [j-1, j-1, j, j],'k','EdgeColor','None');
%             end
%         end
%         paintedNumOfGrid = numCol*(i-1)+j
% end

numGridInIslands = size(islands, 1);

patchVert = zeros(4 * numGridInIslands, 2);
patchFace = zeros(numGridInIslands, 4); %each face is a square, consisting of 
% 4 vertices
for i = 1:numGridInIslands
    patchVert((i-1)*4 + 1, :) = [islands(i,1) - 1, islands(i,2) - 1];
    patchVert((i-1)*4 + 2, :) = [islands(i,1),     islands(i,2) - 1];
    patchVert((i-1)*4 + 3, :) = [islands(i,1),     islands(i,2)];
    patchVert((i-1)*4 + 4, :) = [islands(i,1) - 1, islands(i,2)];
    patchFace(i,:) = [(i-1)*4 + 1, (i-1)*4 + 2, (i-1)*4 + 3, (i-1)*4 + 4];
end
patch('Faces',patchFace,'Vertices',patchVert,'FaceColor','k', 'EdgeColor','None');

