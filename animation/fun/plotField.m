function fieldHandle = plotField(PX, PY, field, minField, maxField)
colormap(jet); % default colromap in matlab with red stands for high value, blue
% means small value
caxis([minField,maxField])
[C,fieldHandle] = contourf(PX,PY,field);
colormap(jet(50));
% hcb = colorbar;
% Ylim=get(hcb,'YLim');% min y and max y for the bar
% set(hcb,'YTick',Ylim,'YTickLabel',{'Low value','High value'})
% set(hcb,'YTickMode','manual')