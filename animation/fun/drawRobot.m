function robot = drawRobot(pos, radius, axisHandle)
axes(axisHandle);
% Draw robot
omega=0:.01:2*pi;
robotX=radius*cos(omega);
robotY=radius*sin(omega);
robot=fill(pos(1)+robotX,pos(2)+robotY,'y');