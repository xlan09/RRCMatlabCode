function fieldUncertainty = uncertaintyMfile(coefficients, xNum, yNum, sysDim, sigma)

fieldUncertainty = zeros(xNum, yNum);
n = sysDim;
for i=1:xNum
        for j=1:yNum
            Cp=reshape(coefficients(i,j,:),1,n);
            M = Cp'*Cp;

            fieldUncertainty(i,j)=trace( M * sigma);
        end
        i
end