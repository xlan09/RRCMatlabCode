#include "mex.h"
#include "Eigen/Dense"

using namespace Eigen;

// when call this function from MATLAB, using
// fieldUncertainty(coefficient, xNum, yNum, sysDim, sigma)
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
#define coefficient_IN prhs[0]
#define xNum_IN prhs[1]
#define yNum_IN prhs[2]
#define sysDim_IN prhs[3]
#define sigma_IN prhs[4]

#define uncertainty_OUT plhs[0]
    // input
    int xNum = (int) mxGetScalar(xNum_IN);
    int yNum = (int) mxGetScalar(yNum_IN);
    int sysDim = (int) mxGetScalar(sysDim_IN);
    double *coeffPtr = mxGetPr(coefficient_IN);
    double *sigmaPtr = mxGetPr(sigma_IN);

    // output
    uncertainty_OUT = mxCreateDoubleMatrix(xNum, yNum, mxREAL);
    double *uncertainty = mxGetPr(uncertainty_OUT);

    // get sigma matrix
    MatrixXd sigma(sysDim, sysDim);
    for (int i = 0; i < sysDim; ++i) {
        for (int j = 0; j < sysDim; ++j) {
            sigma(i,j) = sigmaPtr[i + j * sysDim];
        }
    }

    // calculate uncertainty
    MatrixXd Cp(1, sysDim);
    for (int i = 0; i < xNum; ++i) {
        for (int j = 0; j < yNum; ++j) {

            for (int k = 0; k < sysDim; ++k) {
                Cp(0,k) = coeffPtr[k * (xNum * yNum) + j * xNum + i];
            }
            uncertainty[i + j * xNum] = ((Cp.transpose() * Cp ) * sigma).trace();
        }
    }
}
