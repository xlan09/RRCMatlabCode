function [post_sigma]=riccati(s,world,new_point,previous_sigma)
%Riccati mapping

num_points=world.num_points;

%initialize C matrix
C=zeros(1,num_points);

%% range of the sensor, only used when C=[0 0... 1 ...0]
% radius=min(world.x_interval,world.y_interval)*0.8;
%%
tmp=[world.px',world.py']-ones(num_points,1)*new_point;
distanceSquare=diag(tmp*tmp');
%the square of the distance between each interested point and the new point
%this is used to store all the index of the interested points inside the
%ball centered at the new_point with the above radius

%% one choice for the C matrix
%    IDX=[];
%    %this is used to store all the index of the interested points inside the
%    %ball centered at the new_point with the above radius.
%    for k=1:num_points
%        if distanceSquare(k)<=radius*radius
%         IDX=[IDX;k];
%        end
%    end
% 
% for k=1:size(IDX,1)
%    s.C(IDX(k))=1;
%    % the sensor returns the sum of the scalar field value of those points
%    % which it can measure.
% end
%% another choice for the C matrix
for i=1:num_points
    C(i)=exp(-distanceSquare(i)/(2*6^2))*10;% the sensor returns the sum of the scalar field value of those points which it can measure.
end
%%
A=s.A;
Q=s.Q;
R=s.R;
%riccati equation
post_sigma=A*previous_sigma*A'...
    -A*previous_sigma*C'/(C*previous_sigma*C'+R)*C*previous_sigma*A'+Q;