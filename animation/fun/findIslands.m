function islands = findIslands(environment)
[numRow, numCol] = size(environment);
islands = zeros(numRow * numCol, 2);
count = 0;
for i = 1:numRow
    for j = 1:numCol
        if environment(i,j) == -1 || isnan( environment(i,j) )
          count = count +1;
          islands(count, :) = [i, j];
        end
    end   
end
islands = islands(1:count, :);