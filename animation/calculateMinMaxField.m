clear all;
close all;
clc;

addpath(genpath('./'))
addpath(genpath('../learnOceanTemp/fun/')) % showOcean function
addpath(genpath('../RRC&multRRC/RRC/fun/')) % kalman
addpath(genpath('../plotPaperFig/workspace'))
%*******************************************************************************
% load workspaceGreedyRSRH3000NumActions8Horizon3InitPos400and600.mat
% load workspaceGreedyRSRH3000NumActions8Horizon3InitPos500and200.mat
% load workspaceGreedyRSRH3000NumActions8Horizon4InitPos500and200.mat
load workspaceGreedyRSRH3000NumActions8Horizon4InitPos400and600.mat
%*******************************************************************************
% make sure that the following two parameters are the same with the
% parameters in the kalman function.
sensingScale = 10;
sensingRadius  = 30;
%*******************************************************************************
% Calculate the field
n = world.num_points;

% initialization
RRCSIGMA = zeros(n,n,numSteps);
RRCSIGMA(:,:,1) = initCovMat;
RRCstarSIGMA = zeros(n,n,numSteps);
RRCstarSIGMA(:,:,1) = initCovMat;
RSSIGMA = zeros(n,n,numSteps);
RSSIGMA(:,:,1) = initCovMat;
greedySIGMA = zeros(n,n,numSteps);
greedySIGMA(:,:,1) = initCovMat;
RHSIGMA = zeros(n,n,numSteps);
RHSIGMA(:,:,1) = initCovMat;

for i = 2:numSteps 
    RRCSIGMA(:,:,i) = kalman(sys,world, RRCwaypoints(i,:), RRCSIGMA(:,:,i-1));
    RRCstarSIGMA(:,:,i) = kalman(sys,world, RRCstarWaypoints(i,:), RRCstarSIGMA(:,:,i-1));
    RSSIGMA(:,:,i) = kalman(sys,world, RSwaypoints(i,:), RSSIGMA(:,:,i-1));
    greedySIGMA(:,:,i) = kalman(sys,world, GdyWaypoints(i,:), greedySIGMA(:,:,i-1));
    RHSIGMA(:,:,i) = kalman(sys,world, RHWaypoints(i,:), RHSIGMA(:,:,i-1));
end


% plot the field values in an image with hot color standing for higher value
xMin = 0;
xMax = size(world.environment, 1);
yMin = 0;
yMax = size(world.environment, 2);

% points in the environment
px = 0:xMax;
py = 0:yMax;
xNum = length(px);
yNum = length(py);

% coefficient matrix
% coefficients(:,:,i) is i-th coefficient for all the points
coefficients = zeros(xNum,yNum,n);
[PY,PX]=meshgrid(py,px);

% Calculate coefficients of the linear combination of the basis function
qx=world.px;
qy=world.py;
for i=1:n
   coefficients(:,:,i)=sensingScale * exp((-1)*((PX-qx(i)).^2 + (PY-qy(i)).^2)/(2*sensingRadius^2));
end

% Calculate the estimation uncertainty of the field at each point
% in the environment, which is equal to tr(M(p)*Sigma)
RRCFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCSIGMA(:,:,1) );
RRCstarFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCstarSIGMA(:,:,1) );
RSFU = uncertaintyMex(coefficients, xNum, yNum, n, RSSIGMA(:,:,1) );
greedyFU = uncertaintyMex(coefficients, xNum, yNum, n, greedySIGMA(:,:,1) );
RHFU = uncertaintyMex(coefficients, xNum, yNum, n, RHSIGMA(:,:,1) );

% find the largest and smallest field value in the anmimation, which is
% used to set the color axis.
minField = min( [ min(min(RRCFU)), min(min(RRCstarFU)), min(min(RSFU)), min(min(greedyFU)), min(min(RHFU))] );
maxField = max( [ max(max(RRCFU)), max(max(RRCstarFU)), max(max(RSFU)), max(max(greedyFU)), max(max(RHFU))] );
for i = 2:numSteps
    RRCFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCSIGMA(:,:,i) );
    RRCstarFU = uncertaintyMex(coefficients, xNum, yNum, n, RRCstarSIGMA(:,:,i) );
    RSFU = uncertaintyMex(coefficients, xNum, yNum, n, RSSIGMA(:,:,i) );
    greedyFU = uncertaintyMex(coefficients, xNum, yNum, n, greedySIGMA(:,:,i) );
    RHFU = uncertaintyMex(coefficients, xNum, yNum, n, RHSIGMA(:,:,i) );
    
    minValue = min( [ min(min(RRCFU)), min(min(RRCstarFU)), min(min(RSFU)), min(min(greedyFU)), min(min(RHFU))] );
    maxValue = max( [ max(max(RRCFU)), max(max(RRCstarFU)), max(max(RSFU)), max(max(greedyFU)), max(max(RHFU))] );
    
    if minValue<minField
       minField=minValue; 
    end
    if maxValue>maxField
        maxField=maxValue;
    end
end
workspaceName = ['minMaxField', num2str(numSteps),'NumActions',num2str(numActions),'Horizon',num2str(numStages),'InitPos',num2str(initPos(1)),'and',num2str(initPos(2))];
save(workspaceName);
movefile([workspaceName, '.mat'], './workspace');