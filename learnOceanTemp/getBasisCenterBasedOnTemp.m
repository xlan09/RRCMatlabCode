% get the centroid of each cluster
clear all;
close all;

% add the current folder and all its subfolders to path
addpath(genpath('./'))

%***********************************
% FCM parameter
c = 9;
m = 2;
weightOnTemp = 0;
weightOnGeodist = 1;
%***********************************

% read data
ncFiles = dir('./data/ncomAmseas/*.nc');
filename = ncFiles(1).name;
water_temp = ncread(filename,'water_temp');
lon = ncread(filename,'lon');
lat = ncread(filename,'lat');
size(water_temp)
size(lon)
size(lat)
return

% only use portion of the data
[lonGridDim latGridDim] = size(water_temp(:,:,1));
lonI = 1200;
latJ = 800;
[NEcornerLon, NEcornerLat] = ij2LonLat(filename,lonI,latJ);
[SWcornerLon, SWcornerLat] = ij2LonLat(filename,1,1);

infoFilename = ['oceanInfoLonI',num2str(lonI), 'LatJ',num2str(latJ),'Cluster',num2str(c),...
    'Wtemp', num2str(weightOnTemp),'Wdist',num2str(weightOnGeodist)];
fid = fopen([infoFilename,'.txt'],'w');
fprintf(fid,'Grid dimension in the orginal data is %f by %f.\n',lonGridDim,latGridDim);
fprintf(fid,'Portion of the data is used: lonI is %f, and latJ is %f.\n',lonI,latJ);
fprintf(fid,'Grid interval in longtitude is %f, grid interval in latitude is %f.\n',lon(2)-lon(1),lat(2)-lat(1));
fprintf(fid,'The NE corner position is [%f,   %f] ([longtitude,  latitude]) \n',NEcornerLon,NEcornerLat);
fprintf(fid,'The SW corner position is [%f,   %f] ([longtitude,  latitude]) \n',SWcornerLon,SWcornerLat);

water_temp = water_temp(1:lonI,1:latJ,1);%sea surface temp
[numRow,numCol] = size(water_temp);

% get data
data2Cluster = zeros(numRow*numCol,3);% First column is temp, second and thrid column
% are the i and j index of the temp in the matrix
dataLabel = ones(numRow,numCol);
numData = 0;
for i = 1:numRow
    for j = 1:numCol
        if isnan(water_temp(i,j))
            dataLabel(i,j) = 0; %label the land area as cluster 0
        else
            numData = numData + 1;
            data2Cluster(numData,:) = [water_temp(i,j) i j];% First column is 
%  temp, second and thrid column are the i and j index of the temp in the matrix
        end
    end
end
data2Cluster = data2Cluster(1:numData,:);

% FCM to cluster image
[loopCounter,Center,U,objCost]=fuzzyCmeans(data2Cluster,c,m,weightOnTemp, weightOnGeodist);

% defuzzy, U is a c by numData matrix
[maxMem,IDX]=max(U);
dataCount = 0;
clusterPos = zeros(numRow*numCol,2,c);% store the pos info of the data in each cluster
clusterDataCount = zeros(c,1); % store the number of data in each cluster
water_tempClustered = zeros(numRow,numCol);
environment = zeros(numRow,numCol); %used for RRC
for i=1:numRow
    for j=1:numCol
        if dataLabel(i,j) ~= 0 % cluster 0 is the land, not ocean, no temp for land
            dataCount = dataCount + 1;
            idx=IDX(dataCount);
            dataLabel(i,j)=idx;
            clusterDataCount(idx) = clusterDataCount(idx) + 1;
            clusterPos(clusterDataCount(idx),:,idx) = [i, j];
            
            water_tempClustered(i,j) = Center(idx,1);
            environment(i,j) = 0; %used for RRC
        else
            water_tempClustered(i,j) = NaN; % make the value for the cluster of land to be NaN
            environment(i,j) = -1; % -1 means obstacle
        end
    end
end

% calculate centroid for each cluster
tol = 0.01;
centroid = ones(c,2);
stepSize = 0.0000001;
for i = 1:c
    pos = clusterPos(1:clusterDataCount(i),:,i);
    numDataInIthCluster = size(pos,1)
    ithCentroid = centroid(i,:);
    while 1
        gradient = sum( 2 * (ones(numDataInIthCluster,1) * ithCentroid - pos) )
        ithCentroid = ithCentroid - stepSize * gradient
        if norm(gradient) < tol
            break;
        end
        disp('running gradient descent...')
    end
    centroid(i,:) = ithCentroid;
    disp('######################')
    i
    disp('######################')
end
centroid = round(centroid)

fprintf(fid,'Number of clusters is c = %f, fuzzy parameter is m = %f.\n',c,m);
fprintf(fid,'Centroid for each cluster is:\n');
for i=1:c
    [lonVal,latVal] = ij2LonLat(filename,centroid(i,1), centroid(i,2));
    fprintf(fid,'%f,  %f [Index in water_temp matrix is (%f, %f)]\n',lonVal,latVal,centroid(i,1), centroid(i,2));
end
fclose(fid);
movefile([infoFilename,'.txt'],'./workspace/');
% save workspace
workspaceName = ['basisCenterLonI',num2str(lonI),'LatJ',num2str(latJ),'Cluster',num2str(c),...
    'Wtemp', num2str(weightOnTemp),'Wdist',num2str(weightOnGeodist)];
save([workspaceName,'.mat']);
movefile([workspaceName,'.mat'],'./workspace/');

%% plot ocean
scale = 1;
showOcean(water_tempClustered,scale,'imagesc');
% plot centroid
gcf;
hold on;
for i=1:c
    plot(centroid(i,1)*scale, centroid(i,2)*scale, 's','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(centroid(i,1) + 10, centroid(i,2) + 10, num2str(i))
end

%% savefig is very slow, use the following
frameHandle = getframe(gcf);
figName=['amseasLonI',num2str(lonI),'LatJ',num2str(latJ),'Cluster',num2str(c),...
     'Wtemp', num2str(weightOnTemp),'Wdist',num2str(weightOnGeodist)];
 imwrite(frameHandle.cdata,[figName,'.png'],'png');
 movefile([figName,'.png'],'./pic');

% % save fig
% % save fig may take lots of time since the image is huge
% figName=['amseasLonI',num2str(lonI),'LatJ',num2str(latJ),'Cluster',num2str(c),...
%      'Wtemp', num2str(weightOnTemp),'Wdist',num2str(weightOnGeodist)];
% set(gcf, 'PaperPosition', [-0.48 -0.2 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
% set(gcf, 'PaperSize', [5.05 4.1]); %Set the paper to have width 5 and height 5.
% saveas(gcf, figName, 'pdf') %Save figure
% movefile([figName,'.pdf'],'./pic');