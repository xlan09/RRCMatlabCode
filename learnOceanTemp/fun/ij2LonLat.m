function [lonValue, latValue] = ij2LonLat(filename,i,j)
% Given a grid temp matrix from .nc ocean data, return the lontitude and latitude
% of the position in which the temp is equal to the water_temp(i,j)

lon = ncread(filename,'lon');
lat = ncread(filename,'lat');

lonDiff = lon(2) - lon(1)
latDiff = lat(2) - lat(1)

lonValue = lon(1) + (i-1) * lonDiff;
lonValue = lonValue - 360; % The value is positive in lon, minus 360 to make it negative
latValue = lat(1) + (j-1) * latDiff;

