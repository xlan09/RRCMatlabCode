function showOcean(water_temp,scale,drawFormat)
% scale is the parameter which is used to zoom in and zoom out the area

% show image
figure('name','Ocean Geography');
clf;
axis equal;
hold on;

[numRow,numCol] = size(water_temp);
xMax = numRow * scale;
yMax = numCol * scale;
axis([0 xMax 0 yMax]);

if strcmp(drawFormat,'patch')
    interval = 1 * scale;% interval is the interval between two grid points in x axis
    
    % We will use the patch function with its color resprenting the temp.
    % We will use the default colormap, which is colormap(jet(50)), in this
    % colormap, darkblue stands for low value while hot red stands for high
    % value. The defult caxis is set to be [min(min(water_temp)), max(max(water_temp))]
    %
    % If the water_temp is a 5 by 4 matrix. Then the final image will be like
    % temp14 temp24 temp34 temp44 temp54
    % temp13 temp23 temp33 temp43 temp53
    % temp12 temp22 temp32 temp42 temp52
    % temp11 temp21 temp31 temp41 temp51
    % where tempij is the water_temp(i,j)
    % This representation is the same with the water_temp extracted for the model data,
    % that is, the .nc file.
    for i = 1:numRow
        for j = 1:numCol
            if isnan(water_temp(i,j)) || water_temp(i,j) == -1
                patch([i*scale-interval, i*scale, i*scale, i*scale-interval],...
                    [j*scale-interval, j*scale-interval, j*scale, j*scale], [102 191 127]/255 ,'EdgeColor','None');
            else
                patch([i*scale-interval, i*scale, i*scale, i*scale-interval],...
                    [j*scale-interval, j*scale-interval, j*scale, j*scale], water_temp(i,j),'EdgeColor','None');
            end
        end
        paintedNumOfGrid = numCol*(i-1)+j
    end
    
elseif strcmp(drawFormat, 'imagesc')
    colormap(hot);
    imagesc(water_temp');
else
    error('The last parameter is drawFormat. It can only be patch or imagesc');    
end