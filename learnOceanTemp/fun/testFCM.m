function [imageLabel, newImage] = testFCM(filename,filetype,c,m)
% clear all
% clc;
% 
% addpath ./data
% addpath ./fun
% addpath ./dataClustered

% filename='Bird2ClassGrayGaussian0.2';
% filetype='PNG';
imageData=imread(filename,filetype);
% get rgb values for each pixel in the image
[numRow,numCol,rgb]=size(imageData);
% reshape it into a numRow*numCol by rgb matrix
%           R      G       B
% pixel_11
% pixel_21
% ........
% pixel_numRow1
% pixel_12
% pixel_22
% ........
% pixel_numRow2
% .....
% pixel_1numCol
% pixel_2numCol
% ........
% pixel_numRow_numCol
imageData=reshape(imageData,numRow*numCol,rgb);
imageData=double(imageData);

% Set random seed
str = RandStream.create('mt19937ar','seed',100);
RandStream.setGlobalStream(str);
% c=2;
% m=2;
[loopCounter,Center,U,objCost]=fuzzyCmeans(imageData,c,m);

% defuzzy
[maxMem,IDX]=max(U);
% display clustered image
newImage=zeros(numRow,numCol,rgb);
imageLabel=zeros(numRow,numCol);
for j=1:numCol
    for i=1:numRow
        idx=IDX(numRow*(j-1)+i);
        newImage(i,j,:)=Center(idx,:);
        imageLabel(i,j)=idx;
    end
end


% save image
figure('name','FCM')
newImage=uint8(newImage);
imshow(newImage)
clusterFilename=[filename,'FCM.png'];
imwrite(newImage,clusterFilename,'PNG')

% print clustered image info
infoName=[clusterFilename,'info.txt'];
fID=fopen(infoName,'w+');
fprintf(fID,'Parameters used in FCM to get the corresponding clustered image:\n');
fprintf(fID,'c=%d, m=%d, loopCounter=%d\n',c,m,loopCounter);
fclose(fID);

Center
% move file to tex file folder for report writing
% copyfile(clusterFilename,'~/Dropbox/CS542Project/Figures/')

% move file to clusted folder
movefile(infoName,'./clusteredImage/')
movefile(clusterFilename,'./clusteredImage/')