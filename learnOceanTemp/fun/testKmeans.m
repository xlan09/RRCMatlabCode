function testKmeans(filename,filetype,K)
% Using kmeans to cluster the forest image into several clusters which have
% different forest coverage density
% clc;
% clear all;
% 
% addpath ./data

% filename='Bird2ClassGrayGaussian0.2';
% filetype='PNG';
imageData=imread(filename,filetype);

% get rgb values for each pixel in the image
[numRow,numCol,rgb]=size(imageData);
% reshape it into a numRow*numCol by rgb matrix
%           R      G       B
% pixel_11
% pixel_21
% ........
% pixel_numRow1
% pixel_12
% pixel_22
% ........
% pixel_numRow2
% .....
% pixel_1numCol
% pixel_2numCol
% ........
% pixel_numRow_numCol
imageData=reshape(imageData,numRow*numCol,rgb);
imageData=double(imageData);

% Set random seed
str = RandStream.create('mt19937ar','seed',100);
RandStream.setGlobalStream(str);
% kmeans clustering
% K=2;
[IDX,Center]=kmeans(imageData,K,'Start','uniform','Maxiter', 300);


% display clustered image
newImage=zeros(numRow,numCol,rgb);
imageLabel=zeros(numRow,numCol);
for j=1:numCol
    for i=1:numRow
        idx=IDX(numRow*(j-1)+i);
        newImage(i,j,:)=Center(idx,:);
        imageLabel(i,j)=idx;
    end
end
figure('name','Kmeans')
newImage=uint8(newImage);
imshow(newImage)
% save image
clusterFilename=[filename,'Kmeans.png'];
imwrite(newImage,clusterFilename,'PNG')

% print clustered image info
infoName=[clusterFilename,'info.txt'];
fID=fopen(infoName,'w+');
fprintf(fID,'Parameters used in Kmeans to get the corresponding clustered image:\n');
fprintf(fID,'K=%d\n',K);
fclose(fID);

% move file to tex file folder for report writing
% copyfile(clusterFilename,'~/Dropbox/CS542Project/Figures/')

% % move file to clusted folder
% movefile(infoName,'./dataClustered/')
% movefile(clusterFilename,'./dataClustered/')
