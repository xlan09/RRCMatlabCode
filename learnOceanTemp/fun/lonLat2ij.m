function [indexI, indexJ] = lonLat2ij(filename,lonValue,latValue)
% Given a grid temp matrix from .nc ocean data, the lontitude and latitude
% of one position, return the index of that position in the matrix

lon = ncread(filename,'lon');
lat = ncread(filename,'lat');

lonDiff = lon(2) - lon(1);
latDiff = lat(2) - lat(1);

% The lonValue given is negative, plus 360 to make it positive
indexI = round( (lonValue + 360 - lon(1)) / lonDiff ) + 1;
indexJ = round( (latValue - lat(1)) / latDiff ) + 1;
