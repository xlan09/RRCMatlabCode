function temp = validateData(temp,k)
% Some of the data about the water temperature is not valid. Invalid data
% is set to be 999. This function is to find out these data and use KNN to
% make it valid
% 
% k: parameter for k-nearest-neighbor (KNN)

% make sure that temp is a column vector
[numRow, numCol] = size(temp);
if numRow < numCol
    temp = temp';
end

% assume that the first k data is valid
numData = length(temp);
for i = 1:numData
    if temp(i) == 999
        if i <= k
            error('Please make sure that the first k data is valid!');
        else
            temp(i) = sum(temp(i-k:i-1)) / k;
        end
    end
end