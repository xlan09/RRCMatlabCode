function [loopCounter,centers,U,objCost]=fuzzyCmeans(data,c,m,weightOnTemp, weightOnGeodist)
% data is a N by numFtr matrix, numFtr is number of features.
% c is number of clusters.
% m is fuzziness number, usually chosen as 2.
% 
% centers is a c by numFtr matrix
% 
% U is membership matrix, which is a c by N matrix
% 
% reference: fuzzy c means clustering method Dunn, Joseph C. "A fuzzy
% relative of the ISODATA process and its use in detecting compact
% well-separated clusters." (1973): 32-57.
% 
% Bezdek, James C. "Pattern
% recognition with fuzzy objective function algorithms. Kluwer Academic
% Publishers, 1981. 

% Set random seed
str = RandStream.create('mt19937ar','seed',100);
RandStream.setGlobalStream(str);

[N,numFtr]=size(data);
% initialize U matrix
U=rand(c,N);
U=U./repmat(sum(U),c,1);
% disp('Using Kmeans++ to initialize FCM...')
% initCenter=seeds(data',c);% initCenter is a c by numFea matrix
% U=zeros(c,N);
% for iter2=1:N
%     distance=zeros(c,1);
%     for iter1=1:c
%         distance(iter1)=norm(data(iter2,:)-initCenter(iter1,:));
%     end
%     U(:,iter2)=distance/sum(distance);
% end
% disp('Initializing Done!')

disp('Start FCM...')
tolerance=1e-4;
maxLoop=500;
loopCounter=0;
while 1
    Um=U.^m;
    
    centers=Um*data./repmat(sum(Um,2),1,numFtr);
    % distance between centers and all the data points;
    d = distfunForOceanTemp(centers, data,weightOnTemp,weightOnGeodist);
    % claculate cost function
    objCost=sum(sum(Um.*(d.^2)));
    %     update U matrix
    Unext=zeros(c,N);
    for j=1:c
        Unext(j,:)=1./(d(j,:).^(2/(m-1)).* sum((1./d).^(2/(m-1)),1));
    end
    
    loopCounter=loopCounter+1
    % check stopping criterion
    error=max(max(abs(U-Unext)))
    if error< tolerance  || loopCounter==maxLoop
        if loopCounter==maxLoop
            message=['Max loop number achieved, which is equal to ',num2str(maxLoop),'. Please increase the max loop number to continue!'];
            warning(message);
        end        
        break;
    else
        U=Unext;
    end
end

function out = distfun(center, data)
% calculates the Euclidean distance
%	between each row in CENTER and each row in DATA, and returns a
%	distance matrix OUT of size M by N, where M and N are row
%	dimensions of CENTER and DATA, respectively, and OUT(I, J) is
%	the distance between CENTER(I,:) and DATA(J,:).

out = zeros(size(center, 1), size(data, 1));
% fill the output matrix

if size(center, 2) > 1,
    for k = 1:size(center, 1),
	out(k, :) = sqrt(sum(((data-ones(size(data, 1), 1)*center(k, :)).^2)'));
    end
else	% 1-D data
    for k = 1:size(center, 1),
	out(k, :) = abs(center(k)-data)';
    end
end


function out = distfunForOceanTemp(center, data,weightOnTemp,weightOnGeodist)
% calculates the distance
%	between each row in CENTER and each row in DATA, and returns a
%	distance matrix OUT of size M by N, where M and N are row
%	dimensions of CENTER and DATA, respectively, and OUT(I, J) is
%	the distance between CENTER(I,:) and DATA(J,:).
%   In this case, data is a N by 3 matrix, where data(:,1) is the temp and
%   data(:,2:3) is its position in the 2D plane

out = zeros(size(center, 1), size(data, 1));
% fill the output matrix
if size(center, 2) > 1,
    for k = 1:size(center, 1),
        out(k, :) = weightOnTemp * sqrt(sum(((data(:,1)-ones(size(data, 1), 1)*center(k, 1)).^2)'))...
            + weightOnGeodist * sqrt(sum(((data(:,2:3)-ones(size(data, 1), 1)*center(k, 2:3)).^2)'));
    end
else	% 1-D data
    for k = 1:size(center, 1),
	out(k, :) = abs(center(k)-data)';
    end
end