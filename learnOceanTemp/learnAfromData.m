clear all;
close all;
clc;

addpath(genpath('./'));
%*******************************************************************************
% Parameter
numBasisCenter = 9;
%*******************************************************************************
disp('------------------------------------------');
disp('Learn A matrix from ocean data in 2014...');

mfilestr = mfilename; % get current m filename
logfileName = [mfilestr,'Log.txt'];
logfileID = fopen(logfileName,'w');
fprintf(logfileID,'Observation station ID of the data used to learn A: \n');

% read data from station real-time data
dataFiles = dir('./data/data2014/*.txt');

% construct a cell array to store the temp data
% the reason why using a cell array is that different txt files may have
% different number of data
numDataFiles = length(dataFiles);
tempData = cell(numDataFiles,1);

for i=1:numDataFiles
    filename = dataFiles(i).name
    file = importdata(filename,' ', 2);
    rawData = file.data(:,15);
    tempData{i} = validateData(rawData,3);
    [maxWaterTemp,maxWaterTempIndex] = max(tempData{i})
    fprintf(logfileID,[filename,'\n']);
end

% get the number of data
minNumData = length(tempData{1});
for i = 2:numDataFiles
    numData = length(tempData{i});
    if numData < minNumData
        minNumData = numData;
    end
end
y = zeros(minNumData,numDataFiles);
for i=1:numDataFiles
    y(:,i) = tempData{i}(1:minNumData);
end

% get part of the data, the time step is 'timeInterval' hours
timeInterval = 6;
y = y(1:timeInterval:end,:);

uDim = 1;
u = zeros(size(y,1),uDim);

% % using Overschee's algorithm
% yDim = numDataFiles;
% numBlockRows = 12; 
% [A,K,C,R] = subid(y',[],numBlockRows)

%learn the A matrix using subID
data=iddata(y,u);
opt = n4sidOptions('InitialState','estimate', 'N4weight','MOESP','N4Horizon',[20]);
[sys,x0] = n4sid(data,numBasisCenter,opt)
% eigenvalues
eigenValues = eig(sys.A)
% magnitude of eigenvalues
magnitudeOfEigen = abs(eigenValues)

% close file
fclose(logfileID);
movefile(logfileName,'./simlog');

%*******************************************************************************
% learn the Q and R matrix
% learn R first
A = sys.A;
K = sys.K;
C = sys.C;

% calculate Kalman filter estimated states
numSteps = size(y,1);
sysDim = size(A,1);
states = zeros(sysDim,numSteps);
states(:,1) = A * x0;
for i = 1:numSteps-1
    states(:,i+1) = A * states(:,i);
end
% calculate sensor noise and its covariance matrix and the process noise
% covariance matrix
sensorNoise = y' - C * states;
R = cov(sensorNoise')
diagR = diag(R)
R = sum(diagR) / numBasisCenter

Q = K * R * K'
diagQ = diag(Q)
workspaceName = 'learnAfromData.mat';
save(workspaceName);
movefile(workspaceName,'./workspace/');
