function node=generateRandomNode(world)
% generateRandomNode
% create a random node in the free space(initialize)
NEcorner = world.NEcorner;
SWcorner = world.SWcorner;
NWcorner = [SWcorner(1) - abs( NEcorner(1) - SWcorner(1) );
            NEcorner(2) - abs( NEcorner(2) - SWcorner(2) )];

numRobots=world.numRobots;

% randomly pick configuration
pn       = NWcorner(1) + abs(NEcorner(1) - SWcorner(1)) * rand(1,numRobots);% x coordinate of the random node in the 2-D plane
pe       = NWcorner(2) + abs(NEcorner(2) - SWcorner(2)) * rand(1,numRobots);% y coordinate of the random node in the 2-D plane
leafFlag = 1; % this is used to denote whether it is a leaf. If it is '1', then it is a leaf, otherwise, it is not.
cost     = 0;
node     = [pn, pe, leafFlag, cost, 0];% the last element is used to denote its parent node idx.

% check collision with obstacle
while collision(node, node, world),
    pn       = NWcorner(1) + abs(NEcorner(1) - SWcorner(1)) * rand(1,numRobots);% x coordinate of the random node in the 2-D plane
    pe       = NWcorner(2) + abs(NEcorner(2) - SWcorner(2)) * rand(1,numRobots);% y coordinate of the random node in the 2-D plane
    leafFlag = 1;
    cost     = 0;
    node     = [pn, pe, leafFlag, cost, 0];
end
