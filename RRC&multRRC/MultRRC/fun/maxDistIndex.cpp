//******************************************************************************
// find which robot moves forward with the maximum distance.
// when call the fun in MATLAB, use maxDistIndex(node1Pos, node2Pos, numRobots)
//******************************************************************************

#include "mex.h"
#include <math.h> //pow, sqrt
using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
#define node1Pos_IN prhs[0]
#define node2Pos_IN prhs[1]
#define numRobots_IN prhs[2]

#define index_OUT plhs[0]

    double *node1Pos, *node2Pos, *index;
    int numRobots;
    node1Pos = mxGetPr(node1Pos_IN);
    node2Pos = mxGetPr(node2Pos_IN);
    numRobots = (int) mxGetScalar(numRobots_IN);

    index_OUT = mxCreateDoubleScalar(1);
    index = mxGetPr(index_OUT);

    double dist, maxDistance;
    maxDistance=sqrt( pow(node1Pos[0] - node2Pos[0], 2) +  \
                      pow(node1Pos[0+numRobots] - node2Pos[0+numRobots], 2) );

    for ( int i=1; i<numRobots ; ++i ) {
        dist=sqrt( pow(node1Pos[i] - node2Pos[i], 2) +  \
                   pow(node1Pos[i+numRobots] - node2Pos[i+numRobots], 2) );
        if (dist > maxDistance) {
            maxDistance = dist;
            *index = (double) (i+1);
        }
    }
}

//// find nearest neighbor inside the tree to the newNode
//NN = nearestNeighbor(newNode);
//dist=NN->dist(newNode); // nearest distance
//direction = NN->direction(newNode);//direction to newNode
//direction = normalize(direction);
//
//maxDistIndex = NN->maxDistIndex(newNode);
//maxDist = segmentLen / sqrt( pow(direction[maxDistIndex],2) \
//                             + pow(direction[maxDistIndex + numRobots],2) );
//
//if ( dist > maxDist ) {
////        if the distance between the newNode and the nearest vertex
////        inside the tree is smaller than "maxDist", then we just
////        add this newNode to the tree; Otherwise,
////        we can only extend the tree towards this newNode by a maximum
////        distance given by "maxDist"
//    newNode->position = addVector( NN->position, scaleVector(direction, maxDist) );
//}
