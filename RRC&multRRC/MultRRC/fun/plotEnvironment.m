function plotEnvironment(world)
%plot the environment, the tree and the cycle returned by RRC

%plot the axis
sideLength=world.sideLength;
axis equal;
axis([world.SWcorner(1),world.NEcorner(1),...
    world.SWcorner(2), world.NEcorner(2)]);
hold on;

%plot the obstacles
for i=1:world.NumObstacles,
    X = [world.cn(i)-1/2*sideLength, world.cn(i)-1/2*sideLength, world.cn(i)+1/2*sideLength, world.cn(i)+1/2*sideLength];
    Y = [world.ce(i)-1/2*sideLength, world.ce(i)+1/2*sideLength, world.ce(i)+1/2*sideLength, world.ce(i)-1/2*sideLength];
    fill(X,Y,'k');
end
%plot the points of interest
for i=1:world.num_points
    plot(world.px(i),world.py(i),'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
end
