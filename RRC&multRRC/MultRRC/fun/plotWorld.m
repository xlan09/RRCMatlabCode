function plotWorld(world,path,tree,cycleIndex,scale)
%plot the environment, the tree and the cycle returned by RRC

numRobots=world.numRobots;
%plot the axis
axis equal;
hold on;

environment = world.environment;
[numRow,numCol] = size(environment);
xMax = numRow * scale;
yMax = numCol * scale;
axis([0 xMax 0 yMax]);

interval = 1 * scale;% interval is the interval between two grid points in x axis
% We will use the patch function with its color resprenting the temp.
% We will use the default colormap, which is colormap(jet(50)), in this
% colormap, darkblue stands for low value while hot red stands for high
% value. The defult caxis is set to be [min(min(water_temp)), max(max(water_temp))]
% 
% If the water_temp is a 5 by 4 matrix. Then the final image will be like
% temp14 temp24 temp34 temp44 temp54
% temp13 temp23 temp33 temp43 temp53
% temp12 temp22 temp32 temp42 temp52
% temp11 temp21 temp31 temp41 temp51
% where tempij is the water_temp(i,j)
% This representation is the same with the water_temp extracted for the model data,
% that is, the .nc file. 
% for i = 1:numRow
%     for j = 1:numCol
%         if environment(i,j) == -1
%         patch([i*scale-interval, i*scale, i*scale, i*scale-interval],...
%             [j*scale-interval, j*scale-interval, j*scale, j*scale],'k','EdgeColor','None');
%         end
%     end
%     paintedNumOfGrid = numCol*(i-1)+j
% end
colormap(hot);
imagesc(environment');

%plot the tree
for j=1:numRobots
    for i=1:size(path,2)
        X = path{i}(:,j);
        Y = path{i}(:,j+numRobots);
        plot(X * scale, Y * scale,'color',[100/255 149/255 237/255]);
    end
end


% plot points of interest
for i=1:world.num_points
    plot(world.px(i) * scale, world.py(i)* scale,'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
    text(world.px(i)+10,world.py(i)+10,num2str(i),'color','m','fontsize',14,'fontweight','bold');
end

%plot the cycle returned by RRC
period=size(cycleIndex,1);
X=zeros(period+1,1);
Y=zeros(period+1,1);
for j=1:numRobots
    for i=1:period
        X(i) = tree(cycleIndex(i),j);
        Y(i) = tree(cycleIndex(i),j+numRobots);
    end
    X(period+1)=X(1);
    Y(period+1)=Y(1);
    plot(X * scale, Y * scale,'r','linewidth',2);
    X
    Y
    dist=sqrt((X(2:end)-X(1:end-1)).^2 + (Y(2:end)-Y(1:end-1)).^2)
    disp('----------------------')
end
