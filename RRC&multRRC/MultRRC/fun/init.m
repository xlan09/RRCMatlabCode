function s=init(A, Q, R)
% %initialize for the dynamics of the environment
% 
% %the A matrix of the system, its dimension is determined by the number of
% %interested points
% s.A=eye(num_points)*0.99;
% 
% %measurement system matrix, it is a function of the position of the sensor.
% s.C=[];
% 
% %covariance matrix of system noise
% s.Q=eye(num_points)*5;
% 
% %covariance matrix of measurement noise
% s.R=10;
s.A = A;
s.Q = Q;
s.R = R;
s.C = [];
