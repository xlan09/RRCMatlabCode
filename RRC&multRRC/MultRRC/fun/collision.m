function collision_flag = collision(node1, node2, world)
% collision check to see if a path from node1 to node2 is in
% collsion with obstacles
%
%the inputs:
% node1: row vector;
% node2: row vector;
% world: the environment with obstacles
%
%the outputs:
%collision_flag=1 means collision happens and the path is NOT feasible;
%otherwise, collision_flag=0;

numRobots= world.numRobots;
nodePos1=node1(1:2*numRobots);
nodePos2=node2(1:2*numRobots);
environment = world.environment;

% check collision
collision_flag = 0;

xInterval = 1;
yInterval = 1;
for iter = 1:numRobots
    num_xInterval = ceil( abs(nodePos1(iter) - nodePos2(iter) ) / xInterval );
    num_yInterval = ceil( abs(nodePos1(iter + numRobots) - nodePos2(iter + numRobots) ) / yInterval );
    numPointsBetwn = max(num_xInterval + 1, num_yInterval + 1);
    alpha = linspace(0,1, numPointsBetwn);
    pointsBetwn = ceil( [alpha * nodePos1(iter) + (1-alpha)* nodePos2(iter);...
        alpha * nodePos1(iter + numRobots) + (1-alpha)* nodePos2(iter + numRobots)] );% 2 by numPointsBetwn matrix

    for i = 1:numPointsBetwn
        % In the environment matrix, if one element = -1, then it is an
        % obstacle
        % note pointsBetwn(2,i) corresponds to the first dim of environment, pointsBetwn(1,i)
        % corresponds to the second dim of environment
        if environment(pointsBetwn(1,i), pointsBetwn(2,i)) == -1
            collision_flag = 1;
            break;
        end
    end
    
    if (collision_flag == 1)
        break; % break the outer loop
    end
end