// find cycle index inside the tree
// use cycleIndex=findCycle_mex(treeStruct,treeSize,node1_ID,node2_ID)
//
// The inputs:
// treeStruct: the parent and children relationship inside the random tree, the
// random tree is a treeSize by 5 matrix which each row is a
// node=[px, py, leafFlag, cost, parentID]
// treeStruct is the fifth column of the random tree, that is, tree(:,5).
//
// treeSize must be the size of the tree in which the new node has already been added
//
// node1_ID: the ID of the node inside the ball which is used to form the
// cycle with node2 and the new_node;
// node2_ID: def is the same with node1_ID
//
// The outputs:
// cycleIndex: the index of all the nodes inside the cycle, column vector.

#include <vector>
#include "mex.h"
using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
//  Macros for the ouput and input arguments
#define treeStruct_IN prhs[0]
#define treeSize_IN  prhs[1]
#define node1ID_IN  prhs[2]
#define node2ID_IN  prhs[3]

#define cycleIndex_OUT plhs[0]

// get inputs
    double *treeStruct;
    int treeSize, node1ID, node2ID, parent;
    treeStruct=mxGetPr(treeStruct_IN);
    treeSize=(int) mxGetScalar(treeSize_IN);
    node1ID=(int) mxGetScalar(node1ID_IN);
    node2ID=(int) mxGetScalar(node2ID_IN);

// get the path from node1ID to the root
    vector<int> path1;
    parent=node1ID;
    while(parent>=1) {
        path1.push_back(parent);
        parent=(int) treeStruct[parent-1];
    }

// get the common node of path1 and path2
    vector<int> path2;
    parent=node2ID;
    while(parent>=1) {
        path2.push_back(parent);
        parent=(int) treeStruct[parent-1];
    }

// find the lowest common ancestor
    int commonAncestorIndexInPath1, commonAncestorIndexInPath2, findFlag = 0;
    int path1iter = path1.size()-1, path2iter = path2.size()-1;

    while(path1iter >= 0 && path2iter >= 0) {
        if (path1[path1iter] != path2[path2iter]) {
            // commonAncestor is the last element equal
            commonAncestorIndexInPath1  = path1iter+1;
            commonAncestorIndexInPath2  = path2iter+1;
            findFlag = 1;
            break;
        }
        --path1iter;
        --path2iter;
    }

    // findFlag =0 means node1 and node2 are in the same path to root
    if (findFlag == 0){
        // plus one is because it minus one in the previous loop
        commonAncestorIndexInPath1 = path1iter +1;
        commonAncestorIndexInPath2 = path2iter +1;
    }

    // default common ancestor is the root
//    while (parent>=1 && !findCommonNodeFlag) {
//        for(int i=0; i<path1.size(); i++) {
//            if ( parent== path1[i] ) {
//                path2.push_back(parent);
//                commonAncestor=i;
//                findCommonNodeFlag=1;
//                break;
//            }
//        }
//        if (!findCommonNodeFlag) {
//            path2.push_back(parent);
//            parent=(int) treeStruct[parent-1];
//        }
//    }

// get the cycleIndex
    //plus 1 is to plus the new node, minus 1 is to minus the repeated common
    // ancestor
    int period=commonAncestorIndexInPath1+1 + commonAncestorIndexInPath2+1 -1 +1;
    int *cycleIndex;
    cycleIndex_OUT = mxCreateNumericMatrix(period, 1, mxINT32_CLASS, mxREAL);
    cycleIndex= (int*) mxGetData(cycleIndex_OUT);

    // not include the common ancestor
    for(int i=0; i < commonAncestorIndexInPath1; ++i) {
        cycleIndex[i] = path1[i];
    }
    // include the common ancestor
    for (int i = commonAncestorIndexInPath1; i < period-1; ++i) {
        cycleIndex[i]=path2[commonAncestorIndexInPath1 + commonAncestorIndexInPath2 - i]; // path2 is reversed
    }
    cycleIndex[period-1]=treeSize;
}
