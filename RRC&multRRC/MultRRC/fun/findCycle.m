function [cycleIndex]=findCycle(tree,point1_id,point2_id)
% Find the index for all the vertices inside a cycle
%
%the inputs:
% tree: the tree structure, in which the new_node has already been added to.
% point1_id: the id of the point inside the ball which is used to form the
% cycle with point2 and new_point;
% point2_id: the same with point1_id;
%
%the outputs:
%cycleIndex: the index of all the nodes inside the cycle, column vector

%calculate the length of the path from point1 to the root of the tree
parent=point1_id;
pathLength=0;
while parent>=1
    pathLength=pathLength+1;
    parent=tree(parent,end);
end

%IDX1 is used to store the index of all the nodes in the path
IDX1=zeros(pathLength,1);
parent=point1_id;
i=1;
while parent>=1
    IDX1(i)=parent;
    parent=tree(parent,end);
    i=i+1;
end

%calculate the length of the path from point2 to the root of the tree
parent=point2_id;
pathLength=0;
while parent>=1
    pathLength=pathLength+1;
    parent=tree(parent,end);
end

%IDX2 is used to store the index of all the nodes in the path
IDX2=zeros(pathLength,1);
parent=point2_id;

%find the common node of the two paths
i=1;
common_node_id=1;
flag=1;
while parent>=1 && flag
    IDX2(i)=parent;
    for j=1:size(IDX1,1)
        if IDX2(i)==IDX1(j)
            common_node_id=j;
            flag=0;
            break;
        end
    end
    parent=tree(parent,end);
    i=i+1;
end

%get the cycleIndex
index=IDX2(1:(i-2));
index=index(end:-1:1);
cycleIndex=[IDX1(1:common_node_id);index;size(tree,1)];