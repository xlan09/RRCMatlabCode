function [new_tree,minCycle,minCycle_cost]= extendTree(tree,segmentLength,world,s,minCycle,minCycle_cost)
% Extend tree by randomly selecting point and growing tree toward that point
%

numRobots=world.numRobots;
%calculate the parameter for the RRT
% V that are within a ball of radius r( card (V ) ) =
% min{gama_{RRG} ( log( card (V ) ) / card (V ) )^(1/d), eta}, where eta is the
% constant appearing in the definition of the local steering.
% gama_{RRG} >= gama_{RRG}^* = 2*( 1 + 1/d)^(1/d) ( volume( X_{free} ) /
% volume(unit ball in d-dimensional Euclidean space) )^(1/d)
sampleSpaceVolume=( abs(world.NEcorner(1)-world.SWcorner(1))* abs(world.NEcorner(2)-world.SWcorner(2)) )^numRobots;
unitBallVol = (pi^numRobots)/ factorial(numRobots); 
gamaRRT=2*(1+1/(2*numRobots))^(1/(2*numRobots))*(sampleSpaceVolume/unitBallVol)^(1/(2*numRobots));
% gamaRRT=2*(1+1/2)^(1/2)*(freeVolume/pi)^(1/2)+numRobots;

%extend the tree
%extension_flag=1 means we have extended both trees successfully
extension_flag = 0;
while extension_flag==0
    %% find the potential node
    % select a random point
    randomPoint = generateRandomNode(world);
    randomPoint =randomPoint(1:2*numRobots);
    
    % find leaf inside tree that is closest to the randomPoint
    tmp = tree(:,1:2*numRobots)-ones(size(tree,1),1)*randomPoint;
    % 'dist' is the square of the minimum distance, 'idx' is the id of the
    % nearest node.
    [dist,idx] = min(diag(tmp*tmp'));
    
    direction = (randomPoint-tree(idx,1:2*numRobots));
    direction = direction/norm(direction);
    maxDistRobotIndex = maxDistIndex(randomPoint, tree(idx,1:2*numRobots),numRobots);
    maxDist = segmentLength / sqrt( direction(maxDistRobotIndex)^2 + direction(maxDistRobotIndex + numRobots)^2);
    if dist<= maxDist^2
        new_point=randomPoint;
    else
        new_point = tree(idx,1:2*numRobots)+ direction * maxDist;
    end
    new_node = [new_point, 1, 0, 0]; 
    %%
    %find all the vertices inside the ball centered at the new_node
    if (collision(new_node, tree(idx,:), world)==0)
%         maxDist
%         gamaRRT*(log(size(tree,1))/size(tree,1))^(1/(2*numRobots))
        shrinkRadius = gamaRRT*(log(size(tree,1))/size(tree,1))^(1/(2*numRobots))
        maxDist
        radius=min(shrinkRadius, maxDist);
        
        %% find all the vertices inside the ball for tree
        tmp1=tree(:,1:2*numRobots)-ones(size(tree,1),1)*new_point;
        %the square of the distance between each vertex and the new point
        distanceSquare=diag(tmp1*tmp1');
        %IDX is used to store all the index of the vertices inside the
        %ball centered at the new_point with the above radius
        IDX=[];
        for i=1:size(distanceSquare,1)
            if distanceSquare(i)<=radius*radius
                IDX=[IDX;i];
            end
        end
        
        %% add the potential nodes to the tree
        new_tree = [tree; new_node];
        
        IDXsize=size(IDX,1)
        if IDXsize>1
            %if there are at least two vertices inside the ball
            min_id=idx;%assume that the nearest vertex has the minimum cost
            %"idx" is the id of the nearest vertex.
            min_cost=inf;
            indexOfCycle=[];
            
            newTreeStruct=new_tree(:,end);
            newTreeSize=size(new_tree,1);
            %% check detectability and compare the cost of these cycles
            for i=1:IDXsize-1
                for j=(i+1):IDXsize
                    if collision(new_node,tree(IDX(i),:),world)==0 && collision(new_node,tree(IDX(j),:),world)==0
                        %find the index of the cycle
%                         cycleIndexOld=findCycle(new_tree,IDX(i),IDX(j))
                        cycleIndex=findCycle_mex(newTreeStruct,newTreeSize,IDX(i),IDX(j));
                        %check detectability
%                         detectability_flag=detectabilityCheck(s,new_tree,world,cycleIndex)
%                         detectability_flag=stabilizabilityCheck(s,new_tree,world,cycleIndex)
                            %calculate cycle cost
                            cycle_cost=persistentCost_mex(s,new_tree,world,cycleIndex);                            
                            if  cycle_cost<min_cost
                                min_cost=cycle_cost;
                                min_id=IDX(j);
                                indexOfCycle=cycleIndex;
                            end
                    end
                end
            end

            new_tree(min_id,end-2)=0;%mark it is no longer a leaf
            new_tree(end,end) =min_id;%set the parent node
            
            %Find the min cost cycle in the tree
            if  min_cost<minCycle_cost
                minCycle_cost=min_cost;
                minCycle=indexOfCycle;
            end
        else
            %if there is only one vertex inside the ball, it must be the
            %nearest vertex. Just conncet the new_node to this nearest
            %vertex
            new_tree(idx,end-2)=0;%mark it is no longer a leaf
            new_tree(end,end) = idx;
            
        end
        extension_flag=1;%it is used to denote we have extended the tree
    end
    
end
