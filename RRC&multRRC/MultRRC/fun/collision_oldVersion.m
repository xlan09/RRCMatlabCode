function collision_flag = collision_oldVersion(node, parent, world)
% collision check to see if a path from the node to its parent is in
% collsion with obstacles
%
%the inputs:
% node: 1 by 5 row vector;
% parent: the parent of the node, 1 by 5 row vector;
% world: the environment with obstacles
%
%the outputs:
%collision_flag=1 means collision happens and the path is NOT feasible;
%otherwise, collision_flag=0;

%the maximum distance for the robot to move in the environment
sideLength=world.sideLength;
numRobots=world.numRobots;

collision_flag = 0;

for k=1:numRobots
    %check whether generated random node is outside of the region
    if ((node(k)>=world.NEcorner(1))...
            || (node(k)<=world.SWcorner(1))...
            || (node(k+numRobots)>=world.NEcorner(2))...
            || (node(k+numRobots)<=world.SWcorner(2)))
        collision_flag = 1;
        
    else
        
        for sigma = 0:.01:1,
            p(1) = sigma*node(1,k) + (1-sigma)*parent(1,k);
            p(2)=sigma*node(1,k+numRobots) + (1-sigma)*parent(1,k+numRobots);
            % check whether it collides with each obstacle
            for i=1:world.NumObstacles,
                if (world.cn(i)-1/2*sideLength<=p(1)  &&  p(1)<=world.cn(i)+1/2*sideLength  &&  world.ce(i)-1/2*sideLength<=p(2)  &&  p(2)<=world.ce(i)+1/2*sideLength),
                    collision_flag = 1;
                    break;%break the inter loop
                end
            end
            if collision_flag==1%if it detects collision, break the outer loop
                break;
            end
        end
    end
    
    if collision_flag==1 %if there is one trajectory colliding with the obstacles
        break;
    end
    
end
