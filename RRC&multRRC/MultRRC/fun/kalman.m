function [post_sigma]=kalman(s,world,sensor_pos,previous_sigma)
%Kalman filter
%
%the inputs:
%s: the dynamics of the environment;
%world: the environment with obstacles and points of interest;
%sensor_pos: the sensor position; if 'k' sensors, then a k by 2 matrix
%previous_sigma: the initial priori covariance matrix for the Kalman filter

num_points=world.num_points;

%initialize C matrix
num_sensors=size(sensor_pos,1);
s.C=zeros(num_sensors,num_points);

%get an array to store the distance
distanceSquare=zeros(num_points,1,num_sensors);

%% range of the sensor, only used when C=[0 0... 1 ...0]
%radius=min(world.x_interval,world.y_interval)*0.8;
%%
for i=1:num_sensors
    tmp=[world.px',world.py']-ones(num_points,1)*sensor_pos(i,:);
    distanceSquare(:,:,i)=diag(tmp*tmp');
end
%the square of the distance between each interested point and the new point
%this is used to store all the index of the interested points inside the
%ball centered at the new_point with the above radius

%% one choice for the C matrix
%    IDX=[];%this is used to store all the index of the interested points
%    inside the ball centered at the new_point with the above radius.
%    for k=1:num_points
%        if distanceSquare(k)<=radius*radius;
%        IDX=[IDX;k];
%        end
%    end

%for k=1:size(IDX,1)
%    s.C(IDX(k))=1;% the sensor returns the sum of the scalar field value of those points which it can measure.
%end
%% another choice for the C matrix
for i=1:num_sensors
    distance=distanceSquare(:,:,i);
    for j=1:num_points
        s.C(i,j)=exp(-distance(j)/(2*6^2))*10;% the sensor returns the sum of the scalar field value of those points which it can measure.
    end
end
%%
A=s.A;
C=s.C;
Q=s.Q;
R=s.R * eye(num_sensors);

%riccati equation for the Kalman filter
post_sigma=A*previous_sigma*A'...
    -A*previous_sigma*C'/(C*previous_sigma*C'+R)*C*previous_sigma*A'+Q;
