function composite_cycleIndex=compositeCycle(cycleIndex1,cycleIndex2)
% give the cycle pair

%find composite cycleIndex
period1=size(cycleIndex1,1);
period2=size(cycleIndex2,1);
period=lcm(period1,period2);

composite_cycleIndex=zeros(period,2);
cycleIndex=[];
for iter=1:period/period1
    cycleIndex=[cycleIndex;cycleIndex1];
end
composite_cycleIndex(:,1)=cycleIndex;

cycleIndex=[];
for iter=1:period/period2
    cycleIndex=[cycleIndex;cycleIndex2];
end
composite_cycleIndex(:,2)=cycleIndex;
