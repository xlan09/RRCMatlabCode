%simulation for the RRC algorithm for multiple robots
clear all;
close all;
clc;

addpath(genpath('./'))
addpath(genpath('../../learnOceanTemp'))
%*******************************************************************************
% Load environment data
environmentData = load('basisCenterLonI1200LatJ800Cluster9Wtemp0Wdist1.mat','environment','centroid');
environment = environmentData.environment;
centroid = environmentData.centroid;

%*******************************************************************************
% Load learned A, Q and R
AQRdata = load('learnAfromData.mat','A','Q','R');
A = AQRdata.A;
Q = AQRdata.Q;
R = AQRdata.R;

% Set random seed
seed=100;
str = RandStream.create('mt19937ar','seed',seed);
RandStream.setGlobalStream(str);
%**************************************
% Parameter
% Length between any two nodes.
% 250 works well for 3 robots
% 350 works well for 4 robots
segmentLength = 250;
numRobots = 3;
%**************************************
% create the environment which needs to be monitored
world= createWorld(environment, centroid,numRobots);
%Initialize for the dynamics of the environment
s=init(A, Q, R);

%*******************************************************************************
% RRC for multiple robots
startPos=[500 200];
start_node = zeros(1,2*numRobots+3);
for i=1:numRobots
    start_node(1,i)=startPos(1);
    start_node(1,i+numRobots)=startPos(2);
end
start_node(1,2*numRobots+1)=1;% Denote it is a leaf

numSteps=10001;
tree = start_node;
minCycle=[];
minCycle_cost=inf;

%get an array to store the minimum cost of the cycles inside the tree at
%each strep
minCostArray=zeros(numSteps,1);
Instants=zeros(numSteps,1);

% save workspace
workspaceCount = 0;
interval = 500;
% calculate the time consuming
tic
for i=1:numSteps
    [tree,minCycle,minCycle_cost]= extendTree(tree,segmentLength,world,s,minCycle,minCycle_cost);
    Instants(i)=toc;
    minCostArray(i)=minCycle_cost;
    i
    
    % save workspace
    if (mod(i,interval) == 0)
        filename=[num2str(i),'_RRTsegment',num2str(segmentLength),'_numRobots',num2str(numRobots),'Seed',num2str(seed)];
        workspaceName=['workspace',filename];
        save(workspaceName);
        movefile([workspaceName,'.mat'],'./workspace'); 
        workspaceCount = workspaceCount +1;
    end
end
% remove unncessary saved workspace
% for i =1:workspaceCount
%     filename=[num2str(i*interval),'_RRTsegment',num2str(segmentLength)];
%     workspaceName=['workspace',filename,'.mat'];
%     delete(['./workspace/',workspaceName]);
% end

%output the cost, period and the running time
filename=[num2str(numSteps),'_RRTsegment',num2str(segmentLength),'_numRobots',num2str(numRobots),'Seed',num2str(seed)];
simLogFilename=['simLog',filename];
diary(simLogFilename);
numSteps
toc
minCycle_cost
period=size(minCycle,1)
A=s.A
Q=s.Q
R=s.R
disp('c~N(0,6)*10')
disp('1: without initializing algorithm.');
disp('2: without observability check.');
disp('3: without compareCost theorem.');
diary off;
movefile(simLogFilename, './workspace/');

%% plot the environment and the tree
%find all the paths inside the tree
[path] = findPath(tree);

figure(1);
clf;
scale = 1;
plotWorld(world,path,tree,minCycle,scale);
figName=['multRRC',filename];
% frameH = getframe(gcf);
% imwrite(frameH.cdata,[figName,'.png'],'png');
% movefile([figName,'.png'], './pic')
set(gcf, 'PaperPosition', [-0.48 -0.2 7 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [6.1 4.1]); %Set the paper to have width 5 and height 5.
% saveas(gcf, figName, 'pdf') %Save figure
set(gca, 'LooseInset', [0 0 0 0]); % get rid of the white space around the png fig
print([figName,'.png'],'-dpng','-r400')
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');


%plot the minCycleCost VS. the interation number 
figure(2);
clf;
iterationNumber=1:numSteps;
plot(iterationNumber,minCostArray)
xlabel('Number of iterations')
ylabel('minCycleCost')
title('MinCycleCost VS. number of iterations')
% save fig
figName=['costVSiter',filename];
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

%plot the minCycleCost VS. the running time
figure(3);
clf;
plot(Instants,minCostArray)
xlabel('Computation time(seconds)')
ylabel('minCycleCost')
title('MinCycleCost VS. computation time')
% save fig
figName=['costVStime',filename];
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

% save workspace
workspaceName=['workspace',filename];
save(workspaceName);
movefile([workspaceName,'.mat'],'./workspace');