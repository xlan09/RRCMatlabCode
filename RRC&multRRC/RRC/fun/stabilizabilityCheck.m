function flag=stabilizabilityCheck(s,tree,world,cycleIndex)
% this function is used to check whether a cycle is detectable. Because if
% the cycle(a periodic system) is detectable, there exists a solution to
% the periodic Riccati equation along this cycle. And if the cycle is also
% stabilizable, there exists a unique solution to the periodic Riccati.
%
%the inputs:
%cyleIndex: the cycle
%
%the outputs:
%flag=1 means the cycle is detectable.
%
%Note: the tree must include the potential node.
%
%reference: Chu,EKW and Fan, HY and Lin, WW and Wang, CS,
%"Structure-preserving algorithms for periodic discrete-time algebrai
%Riccati equations, INTERNATIONAL JOURNAL OF CONTROL,V.77, No.8,2004.


%the period of the cycle
period=size(cycleIndex,1);

dimSys=world.num_points;

%get an array to store the equivalent A matrix and B matrix
equiva_A=zeros(dimSys,dimSys,period);
equiva_B=zeros(dimSys,period,period);

% if (A',C') is stabilizable, then (A,C) is detectable
A=(s.A)';

%calculate C matrix 
periodic_C=calculateCmatrix(tree,world,cycleIndex);

%if (equiva_A(:,:,i),equiva_B(:,:,i)) is stabilizable for i=1,2....,period,
%then the period system is stabilizable.
index=1:period;
for i=1:period
    equiva_A(:,:,i)=A^period;
    B=[];
    for j=1:period
        B=[B, A^(period-j)*periodic_C(:,:,index(j))'];      
    end
    equiva_B(:,:,i)=B;
    index=index([end,1:end-1]);
end

%check whether all the pairs (equiva_A(:,:,i), equiva_B(:,:,i)) are
%controllable
%flag=1 means all of them are controllable.
flag=1;
for i=1:period
    matrixRank=rank(ctrb(equiva_A(:,:,i),equiva_B(:,:,i)));
    if matrixRank~=dimSys
       flag=0;
       break;
    end
end
