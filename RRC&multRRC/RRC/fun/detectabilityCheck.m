function flag=detectabilityCheck(s,tree,world,cycleIndex)
% this function is used to check whether a cycle is detectable. Because if
% the cycle(a periodic system) is detectable, there exists a solution to
% the periodic Riccati equation along this cycle
%
%the inputs:
%cyleIndex: the cycle
%
%the outputs:
%flag=1 means the cycle is detectable.
%
%Note: the tree must include the potential node and the system is
%reversible. That is, A matrix is nonsingular.

%the period of the cycle
period=size(cycleIndex,1);

dimSys=world.num_points;
%% calculate the reachability Gramian of A' and C'
% if (A',C') is reachable, then (A,C) is detectable

A=(s.A)';

%calculate C matrix 
periodic_C=calculateCmatrix(tree,world,cycleIndex);

compositePeriodic_C=zeros(1,dimSys,dimSys*period);

for i=0:dimSys-1
    for j=1:period
        compositePeriodic_C(:,:,j+i*period)=periodic_C(:,:,j);
    end
end

gramian=0;
t=dimSys*period;
for i=1:t
   gramian=gramian+A^(t-i)*compositePeriodic_C(:,:,i)'*compositePeriodic_C(:,:,i)*(A^(t-i))';
end

%checke wether the gramian matrix is nonsingular. Since the gramian matrix
%is always semipositive definite, we can only check whether it is positive
%definite
%p=0 means gramian is positive definite. I.e. gramian is nonsingular
[R,p]=chol(gramian);

if p==0
    flag=1;
else
    flag=0;
end
