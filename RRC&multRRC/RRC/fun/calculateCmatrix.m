function periodic_C=calculateCmatrix(tree,world,cycleIndex)
% this function is used to calculate the C matrix because the entries of
% the C matrix are determined by the position of the robot
%
%the inputs:
%tree: the tree structure;
%world: the environment with points of interest;
%cycleIndex: the cycle which the robot moves along
%
%the outputs:
%periodic_C: a 3D array
%
%Note: the tree must be the tree which has already included the potential
%node

%period of the cycle
period=size(cycleIndex,1);
num_points=world.num_points;

%% range of the sensor,only used when C matrix is chosen like [0 0 1 0...0]
% radius=min(world.x_interval,world.y_interval)*0.8;
%%
%Store the C matrix
periodic_C=zeros(1,num_points,period);

%intialize C matrix
C=zeros(1,num_points);

%calculate the C matrix
for i=1:period
    tmp=[world.px',world.py']-ones(num_points,1)*tree(cycleIndex(i),1:2);
    distanceSquare=diag(tmp*tmp');%the square of the distance between each
    %interested point and the new point.
    %% one choice for the C matrix
%       IDX=[];
%       %this is used to store all the index of the interested points inside
%       %the ball centered at the new_point with the above radius
%       for k=1:num_points
%           if distanceSquare(k)<=radius*radius;
%             IDX=[IDX;k];
%           end
%       end
%     
%     for k=1:size(IDX,1)
%       C(IDX(k))=1;
%       % the sensor returns the sum of the scalar field value of those
%       % points which it can measure.
%     end
%     
    %% another choice for the C matrix
    for j=1:num_points
        C(j)=exp(-distanceSquare(j)/(2*30^2))*10;% the sensor returns the sum
        %of the scalar field value of those points which it can measure.
    end
    %%
    periodic_C(:,:,i)=C;
end
