function [tree,minCycle]=initializeTree(world,segmentLength)
%this function is used to initialize the RRC algorithm. That is, we want to
%get a cycle which is detectable so we can use the SDA+SSCA algorithm to
%calculate the infinte cost.
%
%the input argument "world" contains all the points of interest. The points
%of interest form a regular grid and numbered in the following way.
% for exmaple, if we have 9 points of interest:
%   X7--------X8-------X9
%   X4--------X5-------X6
%   X1--------X2-------X3
%the input "segmentLength" is the maximum distance the robot can travel at
%one step;

%the output "tree" is a path from the point last visited to the point first
%visited. The minCycle is the initialized cycle index.

%initialize for the tree and the pathIndex the tree starts at the first
%point of interest and the pathIndex starts with 1

num_points=world.num_points;

%the order to visit these points, this is given by some other algorithms
%such as algorithms used to solve the Travelling Salesman Problem
visit_order=[1,2,3,6,9,8,5,7,4];

points_to_visit=zeros(1,2,num_points);
for i=1:num_points
    points_to_visit(:,:,i)=[world.px(visit_order(i)),world.py(visit_order(i))];
end

tree=[points_to_visit(:,:,1),1,0,0];
%initializing algorithm
for i=1:num_points-1
    start_node=[points_to_visit(:,:,i), 1, 0, 0];
    end_node=[points_to_visit(:,:,i+1), 1, 0, 0];
    [path]=path_by_RRT(world,start_node,end_node,segmentLength);
    tree=[path;tree];
end

%get the path from the last visited point to the first visited point
start_node=[points_to_visit(:,:,num_points), 1, 0, 0];
end_node=[points_to_visit(:,:,1), 1, 0, 0];
[path]=path_by_RRT(world,start_node,end_node,segmentLength);
tree=[path;tree];

tree=tree(2:end,:);%the first row is the same as the last row, both of them
%are the first point visited. So we should get rid of the first row.

%reconstruct a tree
tree_size=size(tree,1);
pos=tree(:,1:2);
leaf=zeros(tree_size,1);
leaf(1)=1;
cost=zeros(tree_size,1);
parent=(tree_size-1:-1:0)';

tree=[pos, leaf, cost, parent];

tree=tree(end:-1:1,:);

minCycle=(1:tree_size)';
