function [new_tree,minCycle,minCycle_cost]= extendTree(tree,segmentLength,world,s,minCycle,minCycle_cost)
%   Extend the tree by randomly selecting point and growing tree toward
%   that point "segmentLength" is the maximum length between two connected
%   nodes inside the tree; "minCylce" is the index of the cycle inside the
%   tree which has the minimum cost; "minCycle_cost" is the cost of the
%   minCycle

%volume of the free space
volume=abs(world.NEcorner(1)-world.SWcorner(1)) * abs(world.NEcorner(2)-world.SWcorner(2));
gamaRRT=2*(1+1/2)^(1/2)*(volume/pi)^(1/2);

%extention_flag used to denote whether we extend the tree successfully. If
%NOT, we need to generate random node again until we extend it successfully
extention_flag = 0;
while extention_flag==0,
    % select a random point
    randomPoint = generateRandomNode(world);
    randomPoint =randomPoint(1:2);
    
    % find leaf inside the tree that is closest to the randomPoint
    tmp = tree(:,1:2)-ones(size(tree,1),1)*randomPoint;
    [dist,idx] = min(diag(tmp*tmp'));
    % 'dist' is the square of the minimum distance, 'idx' is the id of the
    % nearest node.
    if dist<segmentLength^2
        %if the distance between the randomPoint and the nearest vertex
        %inside the tree is smaller than "segmentLength", then we just
        %choose this randomPoint as the node to add to the tree; Otherwise,
        %we can only extend the tree towards this randomPoint by a maximum
        %distance given by "segmentLength"
        new_point=randomPoint;
    else
        new_edge = (randomPoint-tree(idx,1:2));
        new_point = tree(idx,1:2)+new_edge/norm(new_edge)*segmentLength;
    end
    
    %potential node that will be added to the tree
    new_node = [new_point, 1, 0, 0];
    
    %% find which vertex we should connect this new_node to.    
    if collision(new_node, tree(idx,:), world)==0
        %radius of the RRT*
        radius=min(gamaRRT*(log(size(tree,1))/size(tree,1))^(1/2),segmentLength);
        tmp1=tree(:,1:2)-ones(size(tree,1),1)*new_point;
        distanceSquare=diag(tmp1*tmp1');%the square of the distance
        %between each vertex and the new point
        IDX=[];%this is used to store all the index of the vertices
        %inside the ball centered at the new_point with the above radius
        for i=1:size(distanceSquare,1)
            if distanceSquare(i)<=radius*radius
                IDX=[IDX;i];
            end
        end
        IDXsize=size(IDX,1);  

        % add the potential nodes to the tree
        new_tree = [tree; new_node];
        if IDXsize>1
            %if there are at least two vertices inside the ball
            min_id=idx;%assume that the nearest vertex has the minimum cost
            %"idx" is the id of the nearest vertex.
            min_cost=inf;
            indexOfCycle=[];
            
            newTreeStruct=new_tree(:,5);
            newTreeSize=size(new_tree,1);
            %% check detectability and compare the cost of these cycles
            for i=1:IDXsize-1
                for j=(i+1):IDXsize
                    if collision(new_node,tree(IDX(i),:),world)==0 && collision(new_node,tree(IDX(j),:),world)==0
                        %find the index of the cycle
%                         cycleIndexMfun=findCycle(new_tree,IDX(i),IDX(j));
                        cycleIndex=findCycle_mex2(newTreeStruct,newTreeSize,IDX(i),IDX(j));
                        %check detectability
%                         detectability_flag=detectabilityCheck(s,new_tree,world,cycleIndex)
%                         detectability_flag=stabilizabilityCheck(s,new_tree,world,cycleIndex)
                            %calculate cycle cost
%                             cycle_cost=persistentCost(s,new_tree,world,cycleIndex);
                            cycle_cost=persistentCost_mex(s,new_tree,world,cycleIndex);
                            
                            if  cycle_cost<min_cost
                                min_cost=cycle_cost;
                                min_id=IDX(j);
                                indexOfCycle=cycleIndex;
                            end
                       
                    end
                end
            end
            new_tree(min_id,3)=0;%mark it is no longer a leaf
            new_tree(end,5) =min_id;%set the parent node
            
            %Find the min cost cycle in the tree
            if  min_cost<minCycle_cost
                minCycle_cost=min_cost;
                minCycle=indexOfCycle;
            end
        else
            %if there is only one vertex inside the ball, it must be the
            %nearest vertex. Just conncet the new_node to this nearest
            %vertex
            new_tree(idx,3)=0;%mark it is no longer a leaf
            new_tree(end,5) = idx;
            
        end
        extention_flag=1;%it is used to denote we have extended the tree
    end
end
