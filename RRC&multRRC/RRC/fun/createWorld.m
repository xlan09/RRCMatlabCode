function world = createWorld(environment,centroid)
% createWorld
% environment is a matrix
% coordinate is 
% -------------------------> y
% |
% |
% |
% |
% |
% |
% |
% \/
% X

num_points = size(centroid,1);
[xMax, yMax] = size(environment);
NEcorner = [0; yMax];
SWcorner = [xMax; 0];

% check to make sure that the region is nonempty
if (NEcorner(1) == SWcorner(1)) || (NEcorner(2) == SWcorner(2)),
    disp('Not valid corner specifications!')
    world=[];
    return;
    % create world data structure
else  
    world.NEcorner = NEcorner;
    world.SWcorner = SWcorner;
    world.num_points = num_points;
    
    % basis centers
    world.px = centroid(:,1)'; % px must be a row vector, this will be used to calculate the C matrix
    world.py = centroid(:,2)'; % py must be a row vector, this will be used to calculate the C matrix
    
    % obstacle position
    world.environment = environment;
end
