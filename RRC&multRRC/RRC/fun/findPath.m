function [path] = findPath(tree)
%find all the paths inside the tree

connectingNodes = [];
for i=1:size(tree,1),
    if tree(i,3)==1,
        connectingNodes = [connectingNodes; tree(i,:)];
        %connectingNodes include all the leaves
    end
end

%find all the paths inside the tree
for i=1:size(connectingNodes,1)
    path{i} = connectingNodes(i,:);
    parent_node = connectingNodes(i,5);
    
    while parent_node>=1,
        path{i} = [tree(parent_node,:); path{i}];
        parent_node = tree(parent_node,5);
    end
end
