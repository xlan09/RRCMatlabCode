function plotInitializeCycle(world,path)
% plotWorld
% plot obstacles and path
sideLength=world.sideLength;
figure(1), clf
axis([world.SWcorner(1),world.NEcorner(1),...
    world.SWcorner(2), world.NEcorner(2)]);
hold on;

for i=1:world.NumObstacles,
    X = [world.cn(i)-1/2*sideLength, world.cn(i)-1/2*sideLength, world.cn(i)+1/2*sideLength, world.cn(i)+1/2*sideLength];
    Y = [world.ce(i)-1/2*sideLength, world.ce(i)+1/2*sideLength, world.ce(i)+1/2*sideLength, world.ce(i)-1/2*sideLength];
    fill(X,Y,'k');
end

X = path(:,1);
Y = path(:,2);
plot(X,Y,'b','linewidth',1.5);


for i=1:world.num_points
    plot(world.px(i),world.py(i),'gd','linewidth',7);
end
