// find cycle index inside the tree
// use cycleIndex=findCycle_mex(treeStruct,treeSize,node1_ID,node2_ID)
//
// The inputs:
// treeStruct: the parent and children relationship inside the random tree, the
// random tree is a treeSize by 5 matrix which each row is a
// node=[px, py, leafFlag, cost, parentID]
// treeStruct is the fifth column of the random tree, that is, tree(:,5).
//
// treeSize must be the size of the tree in which the new node has already been added
//
// node1_ID: the ID of the node inside the ball which is used to form the
// cycle with node2 and the new_node;
// node2_ID: def is the same with node1_ID
//
// The outputs:
// cycleIndex: the index of all the nodes inside the cycle, column vector.

#include <vector>
#include "mex.h"
using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
//  Macros for the ouput and input arguments
#define treeStruct_IN prhs[0]
#define treeSize_IN  prhs[1]
#define node1ID_IN  prhs[2]
#define node2ID_IN  prhs[3]

#define cycleIndex_OUT plhs[0]

// get inputs
    double *treeStruct;
    int treeSize, node1ID, node2ID, parent;
    treeStruct=mxGetPr(treeStruct_IN);
    treeSize=(int) mxGetScalar(treeSize_IN);
    node1ID=(int) mxGetScalar(node1ID_IN);
    node2ID=(int) mxGetScalar(node2ID_IN);

// get the path from node1ID to the root
    vector<int> path1;
    parent=node1ID;
    while(parent>=1) {
        path1.push_back(parent);
        parent=(int) treeStruct[parent-1];
    }

// get the common node of path1 and path2
    vector<int> path2;
    parent=node2ID;

    int findCommonNodeFlag=0, commonAncestor=0;
    // default common ancestor is the root
    while (parent>=1 && !findCommonNodeFlag) {
        for(int i=0; i<path1.size(); i++) {
            if ( parent== path1[i] ) {
                path2.push_back(parent);
                commonAncestor=i;
                findCommonNodeFlag=1;
                break;
            }
        }
        if (!findCommonNodeFlag) {
            path2.push_back(parent);
            parent=(int) treeStruct[parent-1];
        }
    }

// get the cycleIndex
    int path2Len=path2.size();
    int period=commonAncestor+path2Len+1; //plus 1 is to plus the new node
    int *cycleIndex;
    cycleIndex_OUT = mxCreateNumericMatrix(period, 1, mxINT32_CLASS, mxREAL);
    cycleIndex= (int*) mxGetData(cycleIndex_OUT);

    for(int i=0; i<commonAncestor; i++) {
        cycleIndex[i]=path1[i];
    }

    for (int i=commonAncestor; i<period-1; i++) {
        cycleIndex[i]=path2[commonAncestor+path2Len-1-i]; // path2 is reversed
    }
    cycleIndex[period-1]=treeSize;
}
