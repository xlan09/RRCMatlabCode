function showPosOnMap(environment, indexInMatrix)
% show the positon of one element in the environment matrix on the real map
figure;
clf;
hold on;

environment  = environment';
image(environment(end:-1:1,:))
return
plot(indexInMatrix(1),indexInMatrix(2),'s','markersize',10,'color','r','linewidth',5)
