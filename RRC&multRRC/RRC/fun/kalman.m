function [post_sigma]=kalman(s,world,new_point,previous_sigma)
%Kalman filter
num_points = world.num_points;
px = world.px;
py = world.py;
% make sure they are row vectors
[numRow, numCol] = size(px);
if numRow > numCol
    px = px';
end

[numRow, numCol] = size(py);
if numRow > numCol
    py = py';
end

% the square of the distance between each interested point and the new point
% this is used to store all the index of the interested points inside the
% ball centered at the new_point with the above radius
tmp = [px', py'] - ones(num_points,1) * new_point;
distanceSquare = diag(tmp*tmp');

C = zeros(1,num_points);
for i=1:num_points
    C(i)=exp(-distanceSquare(i)/(2*100^2))*10;% the sensor returns the sum of the scalar field value of those points which it can measure.
end

A=s.A;
Q=s.Q;
R=s.R;
%riccati equation for the Kalman filter
post_sigma=A*previous_sigma*A'...
    -A*previous_sigma*C'/(C*previous_sigma*C'+R)*C*previous_sigma*A'+Q;
