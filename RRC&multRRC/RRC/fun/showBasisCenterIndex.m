%%
% plot points of interest
figure
hold on
[numRow,numCol] = size(environment);
xMax = numRow * scale;
yMax = numCol * scale;
axis([0 xMax 0 yMax]);

colormap(hot)
imagesc(environment')

world.num_points
for i=1:world.num_points
    plot(world.px(i) * scale, world.py(i)* scale,'s','markersize',11,'markerfacecolor',[50/255 205/255 50/255]);
%     annotation('textbox',[world.py(i) * scale / xMax, world.px(i) * scale / yMax, 0.05,0.05],'string',num2str(i));
%*******************************************
% basis center index
%********************************************
    text(world.px(i) * scale +10, world.py(i) * scale+10, num2str(i))
end