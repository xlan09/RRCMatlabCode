% Simulation for the RRC algorithm
% 
% Copyright reserved:
%   Xiaodong Lan and Mac Schwager
clear all;
close all;
clc;

addpath(genpath('./'))
addpath(genpath('../../learnOceanTemp'))
%*******************************************************************************
% Load environment data
environmentData = load('basisCenterLonI1200LatJ800Cluster9Wtemp0Wdist1.mat','environment','centroid');
environment = environmentData.environment;
centroid = environmentData.centroid;

%*******************************************************************************
% Load learned A, Q and R
AQRdata = load('learnAfromData.mat','A','Q','R');
A = AQRdata.A;
Q = AQRdata.Q;
R = AQRdata.R;

%*******************************************************************************
% Set random seed
seed=5489;
str = RandStream.create('mt19937ar','seed',seed);
RandStream.setGlobalStream(str);

% create the environment which needs to be monitored
world= createWorld(environment, centroid);


%Initialize for the dynamics of the environment
s=init(A, Q, R);

% Length between any two nodes.
segmentLength = 50;

%% RRC
%store the minCost returned at each step
start_node = [500 200 1 0 0]; % start at environment(500, 200)
tree=start_node;
minCycle=[];
minCycle_cost=inf;

numSteps=10001;
minCostArray=zeros(numSteps,1);
Instants=zeros(numSteps,1);%Time consuming until each step

% save workspace
workspaceCount = 0;
interval = 500;

tic
for i=1:numSteps
    [tree,minCycle,minCycle_cost]= extendTree(tree,segmentLength,world,s,minCycle,minCycle_cost);
    Instants(i)=toc;
    minCostArray(i)=minCycle_cost;
    i
    
     % save workspace
    if (mod(i,interval) == 0)
        filename=[num2str(i),'_RRTsegment',num2str(segmentLength),'Seed',num2str(seed)];
        workspaceName=['workspace',filename];
        save(workspaceName);
        movefile([workspaceName,'.mat'],'./workspace'); 
        workspaceCount = workspaceCount +1;
    end
end

% % remove unncessary saved workspace
% for i =1:workspaceCount
%     filename=[num2str(i*interval),'_RRTsegment',num2str(segmentLength)];
%     workspaceName=['workspace',filename,'.mat'];
%     delete(['./workspace/',workspaceName]);
% end

%output the cost, period and the running time
filename=[num2str(numSteps),'_RRTsegment',num2str(segmentLength),'Seed',num2str(seed)];
simInfoFilename=['simLog',filename];
diary(simInfoFilename);
numSteps
toc
minCycle_cost
period=size(minCycle,1)
A=s.A
Q=s.Q
R=s.R
disp('c~N(0,6)*10')
disp('1: without initializing algorithm.');
disp('2: without observability check.');
disp('3: without compareCost theorem.');
diary off;
movefile(simInfoFilename,'./workspace');

%% plot the world and the tree
% find all the paths inside the tree
[path] = findPath(tree);

scale = 1;
figure(2);
clf;
plotWorld(world,path,tree,minCycle,scale);

% Save figure
figName=['RRC',filename];
% frameH = getframe(gcf);
% imwrite(frameH.cdata,[figName,'.png'],'png');
% movefile([figName,'.png'], './pic')
% set(gcf, 'PaperPosition', [-0.48 -0.2 7 4.5]); %Position plot at left hand corner with width 5 and height 5.
% set(gcf, 'PaperSize', [6.1 4.1]); %Set the paper to have width 5 and height 5.
% % saveas(gcf, figName, 'pdf') %Save figure
% set(gca,'LooseInset',get(gca,'TightInset'))
set(gca, 'LooseInset', [0 0 0 0]);
print([figName,'.png'],'-dpng','-r400');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.png'],'./pic');

%plot how the minCycleCost changes with the iteration number
figure(3);
clf;
iterationNumber=1:numSteps;
plot(iterationNumber,minCostArray)
xlabel('Number of iterations')
ylabel('minCycleCost')
title('MinCycleCost VS. number of iterations')
% save fig
figName=['costVSiter',filename];
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

%plot how the minCycleCost changes with the computation time
figure(4);
clf;
plot(Instants,minCostArray)
xlabel('Computation time(seconds)')
ylabel('minCycleCost')
title('MinCycleCost VS. computation time')
% save fig
figName=['costVStime',filename];
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');

% save workspace
workspaceName=['workspace',filename];
save(workspaceName);
movefile([workspaceName,'.mat'],'./workspace');