clear all
close all
clc

% add path

%plot how the minCycleCost changes with the iteration number
figure(1);
clf;
hold on;
grid on;
axis([0 10000 0 150])
xlabel('Number of iterations')
ylabel('Cost')
title('Cost VS. number of iterations')
startIter = 1;
endIter = 10000;

data = load('../RRC&multRRC/RRC/workspace/workspace10000_RRTsegment50.mat','minCostArray','numSteps');
numSteps = data.numSteps;
minCostArray = data.minCostArray;

iterationNumber = 1:numSteps;
plot(iterationNumber(startIter:endIter),minCostArray(startIter:endIter),'b')

data = load('../RRCstar&multRRCstar/RRCstar/workspace/workspace10000_RRTsegment50Seed5000.mat','minCostArray','numSteps');
numSteps = data.numSteps;
minCostArray = data.minCostArray;
iterationNumber = 1:numSteps;
plot(iterationNumber(startIter:endIter),minCostArray(startIter:endIter),'r')

data = load('../RRC&multRRC/MultRRC/workspace/workspace10000_RRTsegment250_numRobots3.mat','minCostArray','numSteps');
numSteps = data.numSteps;
minCostArray = data.minCostArray;
iterationNumber = 1:numSteps;
plot(iterationNumber(startIter:endIter),minCostArray(startIter:endIter),'b-.','linewidth',1.5)

data = load('../RRCstar&multRRCstar/MultRRCstar/workspace/workspace10000_RRTsegment250_numRobots3.mat','minCostArray','numSteps');
numSteps = data.numSteps;
minCostArray = data.minCostArray;
iterationNumber = 1:numSteps;
plot(iterationNumber(startIter:endIter),minCostArray(startIter:endIter),'r-.','linewidth',1)
legend('RRC', 'RRCstar', 'RRC for 3 Robots', 'RRCstar for 3 Robots')

% save fig
figName='costVSiterRRC&RRCstar';
set(gcf, 'PaperPosition', [-0.2 0 6 4.5]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [5.6 4.6]); %Set the paper to have width 5 and height 5.
saveas(gcf, figName, 'pdf') %Save figure
copyfile([figName,'.pdf'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures');
movefile([figName,'.pdf'],'./pic');