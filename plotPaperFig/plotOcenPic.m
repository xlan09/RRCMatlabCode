clear all;
close all;
clc;

% add the current folder and all its subfolders to path
addpath(genpath('./'))
addpath(genpath('../RRCstar&multRRCstar/RRCstar/'))
addpath(genpath('../learnOceanTemp/'))

% read data
ncFiles = dir('../learnOceanTemp/data/ncomAmseas/*.nc');
filename = ncFiles(1).name;
water_temp = ncread(filename,'water_temp');
lonI = 1200;
latJ = 800;

% draw ocean
water_temp = water_temp(1:lonI,1:latJ,1);%sea surface temp
figure('name', 'Ocean temp');
axis([0 lonI 0 latJ])
hold on;
imagesc2(water_temp', [min(min(water_temp)), max(max(water_temp))]);

% save ocean pic
figName = 'americanSeaDemo';
set(gcf,'color','w');
frameHandle = getframe(gcf,[30,22.5,500,375]);
imwrite(frameHandle.cdata,[figName,'.png'],'png');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures/')
movefile([figName,'.png'],'./pic');

%% draw example traj
simData = load('workspace10000_RRTsegment50Seed5489.mat','minCycle','tree');
minCycle = simData.minCycle;
tree = simData.tree;
%
numRobots = length(tree(1).position)/2;
period=length(minCycle);
X=zeros(period,1)+1;
Y=zeros(period,1)+1;

for j=1:numRobots
    for i=1:period
        pos = tree(minCycle(i)).position;
        X(i) = pos(j);
        Y(i) = pos(j+numRobots);
    end
    X(period+1)=X(1);
    Y(period+1)=Y(1);
    plot(X,Y,'k','linewidth',2);
end

%% save figure
figName = 'OceanAdWithTraj';
set(gcf,'color','w');
frameHandle = getframe(gcf,[30,22.5,500,375]);
imwrite(frameHandle.cdata,[figName,'.png'],'png');
copyfile([figName,'.png'],'/home/lan/Dropbox/Lan15RRCJournalPaper/Figures/')
movefile([figName,'.png'],'./pic');