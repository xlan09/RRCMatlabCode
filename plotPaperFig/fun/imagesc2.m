function h = imagesc2 (img_data,  Clim)
% a wrapper for imagesc, with some formatting going on for nans

% plotting data. Removing and scaling axes (this is for image plotting)
h = imagesc(img_data, Clim);
set(gcf, 'color', 'w'); % make the figure background color to be white

% The NaNs will be shown the same colour as the axis background colour.
% uncommet the following statement to make the NaNs be displayed the same colour
% as the figure background colour.
% axis image off

% set axis color to be black
set(gca, 'color', [0, 0, 0]);

% setting alpha values
if ndims( img_data ) == 2
  set(h, 'AlphaData', ~isnan(img_data)) % set all the nan to be transparent, so it displays the axis color, which is black
elseif ndims( img_data ) == 3
  set(h, 'AlphaData', ~isnan(img_data(:, :, 1)))
end

if nargout < 1
  clear h
end