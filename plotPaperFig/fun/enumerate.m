function actionList = enumerate(numActions, numStages)
% Ection is 1:numActions
% Each stage we can choose one action from the 1:numActions
% actionList is a numActions^numStages by numStages matrix, each row is one action
    if numStages == 0 || numActions == 0
        error('numStages and numActions can not be zero!');
    end
    if numStages ==1
        actionList = 1:numActions;
        actionList = actionList';
        return
    end
    temp = repmat(enumerate(numActions, numStages - 1), numActions, 1);
    temp2 = repmat(1:numActions, numActions^(numStages-1), 1);
    temp2 = reshape(temp2, numActions^numStages,1);
    actionList = [temp2, temp];
end