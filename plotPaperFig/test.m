%%
clear all;

addpath(genpath('./'))

load workspaceCMP3000NumActions8.mat

pos = initPos;
covMat = initCovMat;

cost = zeros(numSteps,1);
cost(1) = max( eig(initCovMat) );
for i = 2:numSteps
    newPos = RRCwaypoints(i,:)
    covMat
    covMat = kalman(sys,world,newPos, covMat);
    cost(i) = max(eig(covMat));
    cost(i)
    pause
end

figure;
plot(1:numSteps,cost)